package com.mikus.voiceshoppingcart.data.helpers;

import android.content.Context;

import com.mikus.voiceshoppingcart.data.realms.BudgetsRealm;
import com.mikus.voiceshoppingcart.data.realms.ShoppingListRealm;
import com.mikus.voiceshoppingcart.utils.PrettyDateUtil;
import com.mikus.voiceshoppingcart.views.model.DefaultStatItemViewModel;

import java.text.ParseException;
import java.util.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

import androidx.databinding.ObservableArrayList;
import io.realm.Realm;

/**
 * Created by MMIIT on 4/11/2020.
 */
public class StatHelper extends AbstractHelper {

    public StatHelper(Context context) {
        super(context);
    }

    public String getBasketDate(int id) {
        Realm db = Realm.getDefaultInstance();
        String result;
        db.beginTransaction();
        ShoppingListRealm realm = db.where(ShoppingListRealm.class).equalTo("id", id).findFirst();

        result = realm != null ? PrettyDateUtil.toFormat("dd.MM.yyyy", realm.getCreatedAt()) : "";
        db.commitTransaction();
        db.close();

        return result;
    }

    public Double getBudgetByBasketId(int id) {
        Realm db = Realm.getDefaultInstance();
        Double result = 0.0;
        db.beginTransaction();
        ShoppingListRealm realm = db.where(ShoppingListRealm.class).equalTo("id", id).findFirst();
        result = realm.getBudget();
        db.commitTransaction();
        db.close();

        return result;
    }

    public Double getSpentsByBasketId(int id) {
        Realm db = Realm.getDefaultInstance();
        Double result = 0.0;
        db.beginTransaction();
        ShoppingListRealm realm = db.where(ShoppingListRealm.class).equalTo("id", id).findFirst();
        result = realm.getBill();
        db.commitTransaction();
        db.close();

        return result;
    }

    public Double getSpentsByCurrentMonth() {
        Realm db = Realm.getDefaultInstance();
        Double result = 0.0;
        db.beginTransaction();
        List<ShoppingListRealm> list = db.where(ShoppingListRealm.class).greaterThan("createdAt", PrettyDateUtil.getFirstDateOfMonth()).findAll();
        result = list.stream().mapToDouble(ShoppingListRealm::getBill).sum();
        db.commitTransaction();
        db.close();
        return result;
    }

    public Double getBudgetByYear() {
        Realm db = Realm.getDefaultInstance();
        Double result = 0.0;
        db.beginTransaction();

        List<BudgetsRealm> list = db.where(BudgetsRealm.class).equalTo("year", PrettyDateUtil.getActualYear()).findAll();
        result = list.stream().mapToDouble(BudgetsRealm::getBudget).sum();
        db.commitTransaction();
        db.close();
        return result;
    }

    public Double getSpentsByYear() {
        Realm db = Realm.getDefaultInstance();
        Double result = 0.0;
        db.beginTransaction();
        List<ShoppingListRealm> list = db.where(ShoppingListRealm.class)
                .greaterThan("createdAt", PrettyDateUtil.getFirstDateOfYear())
                .lessThan("createdAt", PrettyDateUtil.getLastDateOfYear()).findAll();
        result = list.stream().mapToDouble(ShoppingListRealm::getBill).sum();
        db.commitTransaction();
        db.close();
        return result;
    }

    public ObservableArrayList<DefaultStatItemViewModel> listMonthData() {
        Realm db = Realm.getDefaultInstance();
        int year = PrettyDateUtil.getActualYear();
        ObservableArrayList<DefaultStatItemViewModel> resp = new ObservableArrayList<>();
        db.beginTransaction();
        List<BudgetsRealm> months = db.where(BudgetsRealm.class).equalTo("year", year).findAll();
        for (BudgetsRealm month : months) {
            Date firstDate = new GregorianCalendar(month.getYear(), month.getMonth(), 1).getTime();
            Date lastDate = PrettyDateUtil.getLastDateOfMonth(firstDate);
            List<ShoppingListRealm> list = db.where(ShoppingListRealm.class).greaterThan("createdAt", firstDate)
                    .lessThan("createdAt", lastDate).findAll();
            Double bill = list.stream().mapToDouble(ShoppingListRealm::getBill).sum();
            String names = list.stream().map(ShoppingListRealm::getNote).collect(Collectors.joining(","));
            resp.add(new DefaultStatItemViewModel(month.getMonth() + 1, list.size(), month.getBudget(), bill, names, firstDate, lastDate));
        }
        db.commitTransaction();
        db.close();

        return resp;
    }
}
