package com.mikus.voiceshoppingcart.data.helpers;

import android.content.Context;

import com.mikus.voiceshoppingcart.R;
import com.mikus.voiceshoppingcart.data.Database;
import com.mikus.voiceshoppingcart.data.realms.AccountRealm;
import com.mikus.voiceshoppingcart.data.realms.BudgetsRealm;
import com.mikus.voiceshoppingcart.data.realms.ShoppingItemRealm;
import com.mikus.voiceshoppingcart.data.realms.ShoppingListRealm;
import com.mikus.voiceshoppingcart.utils.PrettyDateUtil;
import com.mikus.voiceshoppingcart.views.model.BasketItemViewModel;
import com.mikus.voiceshoppingcart.views.model.ShoppingItemViewModel;

import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import androidx.databinding.ObservableArrayList;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by MMIIT on 2/25/2020.
 */
public class ShoppingListHelper extends AbstractHelper {

    public ShoppingListHelper(Context context) {
        super(context);
    }

    public void generateTestData(){
        String[] items = new String[]{"carrot","bread", "milk","lemons", "mineral water", "oranges", "flour"};

        ShoppingItemViewModel list = addNewList("Tesco", 30d);
        addItemToList(list.getId(),items[ThreadLocalRandom.current().nextInt(0, items.length)], ThreadLocalRandom.current().nextDouble(0, 10), ThreadLocalRandom.current().nextInt(0, 10), null);
        addItemToList(list.getId(),items[ThreadLocalRandom.current().nextInt(0, items.length)], ThreadLocalRandom.current().nextDouble(0, 10), ThreadLocalRandom.current().nextInt(0, 10), null);
        addItemToList(list.getId(),items[ThreadLocalRandom.current().nextInt(0, items.length)], ThreadLocalRandom.current().nextDouble(0, 10), ThreadLocalRandom.current().nextInt(0, 10), null);
        addItemToList(list.getId(),items[ThreadLocalRandom.current().nextInt(0, items.length)], ThreadLocalRandom.current().nextDouble(0, 10), ThreadLocalRandom.current().nextInt(0, 10), null);
        addItemToList(list.getId(),items[ThreadLocalRandom.current().nextInt(0, items.length)], ThreadLocalRandom.current().nextDouble(0, 10), ThreadLocalRandom.current().nextInt(0, 10), null);
        addItemToList(list.getId(),items[ThreadLocalRandom.current().nextInt(0, items.length)], ThreadLocalRandom.current().nextDouble(0, 10), ThreadLocalRandom.current().nextInt(0, 10), null);
        updateBillInList(list.getId());

        list = addNewList("Tesco", 30d);
        addItemToList(list.getId(),items[ThreadLocalRandom.current().nextInt(0, items.length)], ThreadLocalRandom.current().nextDouble(0, 10), ThreadLocalRandom.current().nextInt(0, 10), null);
        addItemToList(list.getId(),items[ThreadLocalRandom.current().nextInt(0, items.length)], ThreadLocalRandom.current().nextDouble(0, 10), ThreadLocalRandom.current().nextInt(0, 10), null);
        addItemToList(list.getId(),items[ThreadLocalRandom.current().nextInt(0, items.length)], ThreadLocalRandom.current().nextDouble(0, 10), ThreadLocalRandom.current().nextInt(0, 10), null);
        addItemToList(list.getId(),items[ThreadLocalRandom.current().nextInt(0, items.length)], ThreadLocalRandom.current().nextDouble(0, 10), ThreadLocalRandom.current().nextInt(0, 10), null);
        addItemToList(list.getId(),items[ThreadLocalRandom.current().nextInt(0, items.length)], ThreadLocalRandom.current().nextDouble(0, 10), ThreadLocalRandom.current().nextInt(0, 10), null);
        addItemToList(list.getId(),items[ThreadLocalRandom.current().nextInt(0, items.length)], ThreadLocalRandom.current().nextDouble(0, 10), ThreadLocalRandom.current().nextInt(0, 10), null);
        updateBillInList(list.getId());

        list = addNewList("Lidl", 30d);
        addItemToList(list.getId(),items[ThreadLocalRandom.current().nextInt(0, items.length)], ThreadLocalRandom.current().nextDouble(0, 10), ThreadLocalRandom.current().nextInt(0, 10), null);
        addItemToList(list.getId(),items[ThreadLocalRandom.current().nextInt(0, items.length)], ThreadLocalRandom.current().nextDouble(0, 10), ThreadLocalRandom.current().nextInt(0, 10), null);
        addItemToList(list.getId(),items[ThreadLocalRandom.current().nextInt(0, items.length)], ThreadLocalRandom.current().nextDouble(0, 10), ThreadLocalRandom.current().nextInt(0, 10), null);
        addItemToList(list.getId(),items[ThreadLocalRandom.current().nextInt(0, items.length)], ThreadLocalRandom.current().nextDouble(0, 10), ThreadLocalRandom.current().nextInt(0, 10), null);
        addItemToList(list.getId(),items[ThreadLocalRandom.current().nextInt(0, items.length)], ThreadLocalRandom.current().nextDouble(0, 10), ThreadLocalRandom.current().nextInt(0, 10), null);
        updateBillInList(list.getId());

        list = addNewList("Billa", 30d);
        addItemToList(list.getId(),items[ThreadLocalRandom.current().nextInt(0, items.length)], ThreadLocalRandom.current().nextDouble(0, 10), ThreadLocalRandom.current().nextInt(0, 10), null);
        addItemToList(list.getId(),items[ThreadLocalRandom.current().nextInt(0, items.length)], ThreadLocalRandom.current().nextDouble(0, 10), ThreadLocalRandom.current().nextInt(0, 10), null);
        addItemToList(list.getId(),items[ThreadLocalRandom.current().nextInt(0, items.length)], ThreadLocalRandom.current().nextDouble(0, 10), ThreadLocalRandom.current().nextInt(0, 10), null);
        updateBillInList(list.getId());

        list = addNewList("Birthday", 30d);
        addItemToList(list.getId(),items[ThreadLocalRandom.current().nextInt(0, items.length)], ThreadLocalRandom.current().nextDouble(0, 10), ThreadLocalRandom.current().nextInt(0, 10), null);
        addItemToList(list.getId(),items[ThreadLocalRandom.current().nextInt(0, items.length)], ThreadLocalRandom.current().nextDouble(0, 10), ThreadLocalRandom.current().nextInt(0, 10), null);
        addItemToList(list.getId(),items[ThreadLocalRandom.current().nextInt(0, items.length)], ThreadLocalRandom.current().nextDouble(0, 10), ThreadLocalRandom.current().nextInt(0, 10), null);
        updateBillInList(list.getId());
    }
    public ShoppingItemViewModel addNewList(String note, Double budget) {
        Double globalBudget = Database.account().readBudget();
        createNewBudget(globalBudget);
        Realm db = Realm.getDefaultInstance();
        ShoppingItemViewModel resp;
        db.beginTransaction();
        ShoppingListRealm list = create(ShoppingListRealm.class);
        list.setCreatedAt(new Date());
        list.setNote(note);

        list.setBudget(budget != null ? budget : 0d);
        resp = new ShoppingItemViewModel(
                list.getId(),
                PrettyDateUtil.toFormat("dd.MM.yyyy", list.getCreatedAt()),
                "",
                list.getBudget(),
                0d,
                note);
        db.commitTransaction();
        db.close();


        return resp;
    }

    public void createNewBudget(Double budget) {
        Realm db = Realm.getDefaultInstance();
        db.beginTransaction();
        int year = PrettyDateUtil.getActualYear();
        int month = PrettyDateUtil.getActualMonth();
        BudgetsRealm existing = db.where(BudgetsRealm.class).equalTo("year", year).equalTo("month", month).findFirst();
        if (existing != null) {
            existing.setBudget(budget);
        } else {
            BudgetsRealm realm = create(BudgetsRealm.class);
            realm.setBudget(budget);
            realm.setMonth(month);
            realm.setYear(year);
        }
        db.commitTransaction();
        db.close();
    }

    public String getShoppingListNote(int id) {
        String note = null;
        Realm db = Realm.getDefaultInstance();
        db.beginTransaction();
        ShoppingListRealm realm = db.where(ShoppingListRealm.class).equalTo("id", id).findFirst();
        if (realm != null) {
            note = realm.getNote();
        }
        db.commitTransaction();
        db.close();

        return note;
    }

    public void updateNoteInList(int id, String note) {
        Realm db = Realm.getDefaultInstance();
        db.beginTransaction();
        ShoppingListRealm realm = db.where(ShoppingListRealm.class).equalTo("id", id).findFirst();
        if (realm != null) {
            realm.setNote(note);
        }
        db.commitTransaction();
        db.close();

    }

    public ObservableArrayList<ShoppingItemViewModel> listShoppingCarts(Date from, Date to) {
        Realm db = Realm.getDefaultInstance();
        ObservableArrayList<ShoppingItemViewModel> items = new ObservableArrayList<>();
        db.beginTransaction();

        RealmResults<ShoppingListRealm> realms;
        if (from != null && to != null) {
            realms = db.where(ShoppingListRealm.class).greaterThan("createdAt", from).lessThan("createdAt", to).findAll();
        } else {
            realms = db.where(ShoppingListRealm.class).findAll();
        }
        for (ShoppingListRealm item : realms) {
            items.add(new ShoppingItemViewModel(
                    item.getId(),
                    PrettyDateUtil.toFormat("dd.MM.yyyy", item.getCreatedAt()),
                    item.toItemsString(),
                    item.getBudget(),
                    item.getBill(),
                    item.getNote()
            ));
        }
        db.commitTransaction();
        db.close();
        return items;
    }

    public ObservableArrayList<ShoppingItemViewModel> listShoppingCarts() {
        return listShoppingCarts(PrettyDateUtil.getFirstDateOfMonth(), PrettyDateUtil.getLastDateOfMonth());
    }

    public ObservableArrayList<BasketItemViewModel> listItems(int id, BasketItemViewModel.IBasketItemChangedListener listener) {
        return listItems(id, false, listener);
    }

    public ObservableArrayList<BasketItemViewModel> listItems(int id, boolean onlyBought, BasketItemViewModel.IBasketItemChangedListener listener) {
        Realm db = Realm.getDefaultInstance();
        ObservableArrayList<BasketItemViewModel> items = new ObservableArrayList<>();
        db.beginTransaction();
        ShoppingListRealm list = db.where(ShoppingListRealm.class).equalTo("id", id).findFirst();


        if (list != null) {
            for (ShoppingItemRealm item : list.getItems()) {
                if (!onlyBought) {
                    items.add(new BasketItemViewModel(id, item.getId(), item.isInBasket(), item.getName(), item.getPrice(), item.getQuantity(), listener));
                } else if (item.isInBasket()) {
                    items.add(new BasketItemViewModel(id, item.getId(), item.isInBasket(), item.getName(), item.getPrice(), item.getQuantity(), listener));
                }
            }
        }
        db.commitTransaction();
        db.close();
        items.sort((t, t1) -> Boolean.compare(!t.isInBasket(), !t1.isInBasket()));
        return items;
    }

    public void addToBasket(boolean inBasket, int id) {
        Realm db = Realm.getDefaultInstance();
        db.beginTransaction();
        ShoppingItemRealm item = db.where(ShoppingItemRealm.class).equalTo("id", id).findFirst();
        if (item != null) {
            item.setInBasket(inBasket);
        }
        db.commitTransaction();
        db.close();
    }

    public BasketItemViewModel addItemToList(int id, String name, BasketItemViewModel.IBasketItemChangedListener listener) {
        return addItemToList(id, name, 0d, 1, listener);
    }

    public BasketItemViewModel addItemToList(int id, String name, Double price, BasketItemViewModel.IBasketItemChangedListener listener) {
        return addItemToList(id, name, price, 1, listener);
    }

    public BasketItemViewModel addItemToList(int id, String name, Double price, Integer quantity, BasketItemViewModel.IBasketItemChangedListener listener) {
        Realm db = Realm.getDefaultInstance();
        BasketItemViewModel resp = null;
        db.beginTransaction();
        ShoppingListRealm list = db.where(ShoppingListRealm.class).equalTo("id", id).findFirst();
        if (list != null) {
            ShoppingItemRealm item = create(ShoppingItemRealm.class);
            item.setName(name);
            item.setPrice(price);
            item.setQuantity(quantity);
            list.getItems().add(item);
            list.setBill(list.toBill());
            resp = new BasketItemViewModel(list.getId(), item.getId(), false, name, price, quantity, listener);

        }
        db.commitTransaction();
        db.close();

        return resp;
    }

    public void updateBudgetInList(int id, Double budget) {
        Realm db = Realm.getDefaultInstance();
        db.beginTransaction();
        ShoppingListRealm realm = db.where(ShoppingListRealm.class).equalTo("id", id).findFirst();
        if (realm != null) {
            realm.setBudget(budget);
        }
        db.commitTransaction();
        db.close();
    }

    public void updateBillInList(int id) {
        Realm db = Realm.getDefaultInstance();
        db.beginTransaction();
        ShoppingListRealm realm = db.where(ShoppingListRealm.class).equalTo("id", id).findFirst();
        if (realm != null) {
            realm.setBill(realm.toBill());
        }
        db.commitTransaction();
        db.close();
    }

    public void updateItemInList(int id, String name, Double price, Integer quantity) {
        Realm db = Realm.getDefaultInstance();
        db.beginTransaction();
        ShoppingItemRealm item = db.where(ShoppingItemRealm.class).equalTo("id", id).findFirst();
        item.setQuantity(quantity);
        item.setPrice(price);
        item.setName(name);
        item.setInBasket(price != null);
        db.commitTransaction();
        db.close();
    }


    public void removeShoppingList(int id) {
        Realm db = Realm.getDefaultInstance();
        db.beginTransaction();
        ShoppingListRealm list = db.where(ShoppingListRealm.class).equalTo("id", id).findFirst();
        RealmList<ShoppingItemRealm> items = list.getItems();
        items.deleteAllFromRealm();
        list.deleteFromRealm();
        db.commitTransaction();
        db.close();
    }

    public void removeItemFromList(int id) {
        Realm db = Realm.getDefaultInstance();
        db.beginTransaction();
        ShoppingItemRealm item = db.where(ShoppingItemRealm.class).equalTo("id", id).findFirst();
        if (item != null) {
            item.deleteFromRealm();
        }
        db.commitTransaction();
        db.close();
    }
}
