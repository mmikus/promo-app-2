package com.mikus.voiceshoppingcart.data;

import android.content.ContentResolver;
import android.content.Context;

import io.realm.DynamicRealm;
import io.realm.DynamicRealmObject;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;

/**
 * Created by MMIIT on 2/23/2020.
 */
public class DatabaseUpdate implements RealmMigration {
    private final Context mContext;

    public DatabaseUpdate(Context context) {
        this.mContext = context;
    }

    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        RealmSchema schema = realm.getSchema();
        if (oldVersion < newVersion && newVersion == 2) {
            schema.get("AccountRealm").addField("wizardStep", Integer.class)
                    .transform(obj -> obj.setInt("wizardStep", 0));
            oldVersion++;
        }
    }
}
