package com.mikus.voiceshoppingcart.data.helpers;

import android.content.Context;
import android.os.LocaleList;

import com.mikus.voiceshoppingcart.data.Database;
import com.mikus.voiceshoppingcart.data.dto.PositionDto;
import com.mikus.voiceshoppingcart.data.realms.AccountRealm;

import io.realm.Realm;

/**
 * Created by MMIIT on 2/23/2020.
 */
public class AccountHelper extends AbstractHelper {

    public AccountHelper(Context context) {
        super(context);
    }

    public boolean hasAccount() {
        Realm db = Realm.getDefaultInstance();
        db.beginTransaction();
        long accounts = db.where(AccountRealm.class).count();
        db.commitTransaction();
        db.close();
        return accounts > 0;
    }

    public void createAccount(String name, Double budget) {
        Realm db = Realm.getDefaultInstance();
        db.beginTransaction();
        String locale = LocaleList.getDefault().get(0).toLanguageTag();
        AccountRealm realm = create(AccountRealm.class);

        realm.setName(name.substring(0, 1).toUpperCase() + name.substring(1));
        realm.setLocale(locale);
        realm.setBudget(budget);
        db.commitTransaction();
        db.close();
    }

    public void updateName(String name) {
        Realm db = Realm.getDefaultInstance();
        db.beginTransaction();
        AccountRealm realm = db.where(AccountRealm.class).findFirst();
        realm.setName(name);
        db.commitTransaction();
        db.close();
    }

    public void updateBudget(double budget) {
        Realm db = Realm.getDefaultInstance();
        db.beginTransaction();
        AccountRealm realm = db.where(AccountRealm.class).findFirst();
        realm.setBudget(budget);
        db.commitTransaction();
        db.close();
        Database.list().createNewBudget(budget);
    }

    public void updateLocale(String locale) {
        Realm db = Realm.getDefaultInstance();
        db.beginTransaction();
        AccountRealm realm = db.where(AccountRealm.class).findFirst();
        realm.setLocale(locale);
        db.commitTransaction();
        db.close();
    }

    public String readName() {
        Realm db = Realm.getDefaultInstance();
        db.beginTransaction();
        AccountRealm account = db.where(AccountRealm.class).findFirst();
        String name = account != null ? account.getName() : "";
        db.commitTransaction();
        db.close();

        return name;
    }

    public String readSpeechLocale() {
        Realm db = Realm.getDefaultInstance();
        String locale = "sk-SK";
        db.beginTransaction();
        AccountRealm account = db.where(AccountRealm.class).findFirst();
        locale = account.getLocale();
        db.commitTransaction();
        db.close();

        return locale;
    }

    public Double readBudget() {
        Realm db = Realm.getDefaultInstance();
        double budget = 0d;
        db.beginTransaction();
        AccountRealm realm = db.where(AccountRealm.class).findFirst();
        if (realm != null) {
            budget = realm.getBudget();
        }
        db.commitTransaction();
        db.close();
        return budget;
    }

    public PositionDto readDashBoardBtnPosition() {
        Realm db = Realm.getDefaultInstance();
        db.beginTransaction();
        AccountRealm realm = db.where(AccountRealm.class).findFirst();
        PositionDto resp = new PositionDto(realm.getDashBtnX(), realm.getDashBtnY());
        db.commitTransaction();
        db.close();

        return resp;
    }

    public PositionDto readStatBtnPosition() {
        Realm db = Realm.getDefaultInstance();
        db.beginTransaction();
        AccountRealm realm = db.where(AccountRealm.class).findFirst();
        PositionDto resp = new PositionDto(realm.getStatBtnX(), realm.getStatBtnY());
        db.commitTransaction();
        db.close();

        return resp;
    }

    public PositionDto readListBtnPosition() {
        Realm db = Realm.getDefaultInstance();
        db.beginTransaction();
        AccountRealm realm = db.where(AccountRealm.class).findFirst();
        PositionDto resp = new PositionDto(realm.getLstBtnX(), realm.getLstBtnY());
        db.commitTransaction();
        db.close();

        return resp;
    }

    public void saveDashBoardBtnPosition(float x, float y) {
        Realm db = Realm.getDefaultInstance();
        db.beginTransaction();
        AccountRealm realm = db.where(AccountRealm.class).findFirst();
        realm.setDashBtnX(x);
        realm.setDashBtnY(y);
        db.commitTransaction();
        db.close();
    }

    public void saveListBtnPosition(float x, float y) {
        Realm db = Realm.getDefaultInstance();
        db.beginTransaction();
        AccountRealm realm = db.where(AccountRealm.class).findFirst();
        realm.setLstBtnX(x);
        realm.setLstBtnX(y);
        db.commitTransaction();
        db.close();
    }

    public void saveStatPosition(float x, float y) {
        Realm db = Realm.getDefaultInstance();
        db.beginTransaction();
        AccountRealm realm = db.where(AccountRealm.class).findFirst();
        realm.setStatBtnX(x);
        realm.setStatBtnY(y);
        db.commitTransaction();
        db.close();
    }

    public int getWizartStep() {
        int result = 0;
        Realm db = Realm.getDefaultInstance();
        db.beginTransaction();
        AccountRealm realm = db.where(AccountRealm.class).findFirst();
        if (realm != null && realm.getWizardStep() != null) {
            result = realm.getWizardStep();
        }
        db.commitTransaction();
        db.close();

        return result;
    }

    public void refreshWizard() {
        Realm db = Realm.getDefaultInstance();
        db.beginTransaction();
        AccountRealm realm = db.where(AccountRealm.class).findFirst();
        realm.setWizardSteps(0);
        db.commitTransaction();
        db.close();

    }

    public void saveWizardStep(int step) {
        Realm db = Realm.getDefaultInstance();
        db.beginTransaction();
        AccountRealm realm = db.where(AccountRealm.class).findFirst();
        int storedStep = realm.getWizardStep() == null ? 0 : realm.getWizardStep();
        if (realm != null && storedStep < step) {
            realm.setWizardSteps(step);
        }
        db.commitTransaction();
        db.close();

    }

}
