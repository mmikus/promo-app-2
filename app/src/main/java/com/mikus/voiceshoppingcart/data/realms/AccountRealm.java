package com.mikus.voiceshoppingcart.data.realms;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by MMIIT on 2/23/2020.
 */
public class AccountRealm extends RealmObject {

    @PrimaryKey
    private int id;

    private String name;
    private String locale;
    private Double budget;

    private float dashBtnX;
    private float dashBtnY;

    private float lstBtnX;
    private float lstBtnY;

    private float statBtnX;
    private float statBtnY;

    private Integer wizardStep;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getBudget() {
        return budget;
    }

    public void setBudget(Double budget) {
        this.budget = budget;
    }

    public float getDashBtnX() {
        return dashBtnX;
    }

    public void setDashBtnX(float dashBtnX) {
        this.dashBtnX = dashBtnX;
    }

    public float getDashBtnY() {
        return dashBtnY;
    }

    public void setDashBtnY(float dashBtnY) {
        this.dashBtnY = dashBtnY;
    }

    public float getLstBtnX() {
        return lstBtnX;
    }

    public void setLstBtnX(float lstBtnX) {
        this.lstBtnX = lstBtnX;
    }

    public float getLstBtnY() {
        return lstBtnY;
    }

    public void setLstBtnY(float lstBtnY) {
        this.lstBtnY = lstBtnY;
    }

    public float getStatBtnX() {
        return statBtnX;
    }

    public void setStatBtnX(float statBtnX) {
        this.statBtnX = statBtnX;
    }

    public float getStatBtnY() {
        return statBtnY;
    }

    public void setStatBtnY(float statBtnY) {
        this.statBtnY = statBtnY;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }


    public Integer getWizardStep() {
        return wizardStep;
    }

    public void setWizardSteps(Integer wizardStep) {
        this.wizardStep = wizardStep;
    }

}
