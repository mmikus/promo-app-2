package com.mikus.voiceshoppingcart.data.helpers;

import android.content.Context;

import io.realm.Realm;
import io.realm.RealmObject;

/**
 * Created by MMIIT on 2/23/2020.
 */
public abstract class AbstractHelper {
    private Context mContext;

    protected AbstractHelper(Context context) {
        this.mContext = context;
    }

    protected Context getContext() {
        return this.mContext;
    }

    protected <E extends RealmObject> E create(Class<E> clazz, Realm realm) {
        Number max = realm.where(clazz).max("id");
        long primaryKey = 1;
        if (max != null) {
            primaryKey = max.longValue() + 1;
        }
        return realm.createObject(clazz, primaryKey);
    }

    protected <E extends RealmObject> E create(Class<E> clazz) {
        return create(clazz, Realm.getDefaultInstance());
    }
}
