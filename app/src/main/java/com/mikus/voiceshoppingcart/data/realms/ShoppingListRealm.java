package com.mikus.voiceshoppingcart.data.realms;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by MMIIT on 2/25/2020.
 */
public class ShoppingListRealm extends RealmObject {

    @PrimaryKey
    private int id;
    private Date createdAt;
    private double budget = 0d;
    private double bill = 0d;
    private String note;
    private RealmList<ShoppingItemRealm> items;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getNote() {
        return note;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public double getBudget() {
        return budget;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }

    public double getBill() {
        return bill;
    }

    public void setBill(double bill) {
        this.bill = bill;
    }

    public RealmList<ShoppingItemRealm> getItems() {
        return items;
    }

    public void setItems(RealmList<ShoppingItemRealm> items) {
        this.items = items;
    }

    public String toItemsString() {
        if (this.getItems() != null && this.getItems().size() > 0) {
            return this.getItems().stream().map(ShoppingItemRealm::getName).collect(Collectors.joining(","));
        }
        return "";
    }

    public Double toBill() {
        double sum = 0d;
        if (this.getItems() != null && this.getItems().size() > 0) {
            for (ShoppingItemRealm item : this.getItems()) {
                if (item.isInBasket()) {
                    sum += item.getPrice() * item.getQuantity().doubleValue();
                }
            }
        }
        return sum;
    }
}
