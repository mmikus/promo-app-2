package com.mikus.voiceshoppingcart.data;


import android.content.Context;

import com.mikus.voiceshoppingcart.data.helpers.AccountHelper;
import com.mikus.voiceshoppingcart.data.helpers.ShoppingListHelper;
import com.mikus.voiceshoppingcart.data.helpers.StatHelper;

import io.realm.DynamicRealm;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by MMIIT on 2/23/2020.
 */
public class Database {
    private static volatile Context mContext;
    private static volatile AccountHelper mAccount;
    private static volatile ShoppingListHelper mList;
    private static volatile StatHelper mStat;

    public static void init(Context context) {
        Database.mContext = context;
        mAccount = null;
        mList = null;
        mStat = null;
    }

    public static StatHelper stat() {
        if (mStat == null) {
            mStat = new StatHelper(mContext);
        }

        return mStat;
    }

    public static AccountHelper account() {
        if (mAccount == null) {
            mAccount = new AccountHelper(mContext);
        }

        return mAccount;
    }

    public static ShoppingListHelper list() {
        if (mList == null) {
            mList = new ShoppingListHelper(mContext);
        }

        return mList;
    }
}
