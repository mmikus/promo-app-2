package com.mikus.voiceshoppingcart.data.dto;

/**
 * Created by MMIIT on 4/23/2020.
 */
public class PositionDto {
    private float x;
    private float y;

    public PositionDto() {
    }

    public PositionDto(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }
}
