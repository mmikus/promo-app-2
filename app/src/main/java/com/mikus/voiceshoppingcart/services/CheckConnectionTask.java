package com.mikus.voiceshoppingcart.services;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.mikus.voiceshoppingcart.utils.Permissions;

/**
 * Created by MMIIT on 6/11/2020.
 */

public class CheckConnectionTask extends AsyncTask<Void, Void, Boolean> {

    private ProgressDialog mDialog;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mDialog = new ProgressDialog(ApplicationLifecycleService.getCurrentActivity());
        mDialog.setMessage("Please Wait");
        mDialog.setCancelable(false);
        mDialog.show();
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        return Permissions.hasInternetConnection();
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }
}
