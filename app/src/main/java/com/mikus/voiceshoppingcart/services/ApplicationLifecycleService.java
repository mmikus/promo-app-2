package com.mikus.voiceshoppingcart.services;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.os.Bundle;

/**
 * Created by MMIIT on 2/26/2020.
 */
public class ApplicationLifecycleService implements Application.ActivityLifecycleCallbacks {

    private static boolean mOnBackground = false;
    private static boolean mIsServiceRunning = false;
    private static boolean mIsRegistered = false;
    public static boolean mStarted = false;
    private static final int NOTIFICATION_JOB = 0;

    private static Activity mCurrentActivity;

    public ApplicationLifecycleService() {
        //  mIsRegistered = Database.registration().isRegistered();
    }

    public static void setRegistered(boolean isRegistered) {
        mIsRegistered = isRegistered;
    }

    public static Activity getCurrentActivity() {
        return mCurrentActivity;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        mCurrentActivity = activity;
    }

    @Override
    public void onActivityStarted(Activity activity) {
    }


    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        //activity.stopService(new Intent(activity, XmppService.class));
        //  mIsServiceRunning = false;
    }


    private boolean isBackground() {
        ActivityManager.RunningAppProcessInfo myProcess = new ActivityManager.RunningAppProcessInfo();
        ActivityManager.getMyMemoryState(myProcess);
        return myProcess.importance != ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND;
    }
}
