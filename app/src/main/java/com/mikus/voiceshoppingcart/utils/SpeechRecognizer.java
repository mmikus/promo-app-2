package com.mikus.voiceshoppingcart.utils;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;

import com.mikus.voiceshoppingcart.R;
import com.mikus.voiceshoppingcart.data.Database;
import com.mikus.voiceshoppingcart.dialogs.SpeechRecognizerDialog;
import com.mikus.voiceshoppingcart.services.ApplicationLifecycleService;

import java.util.List;

import androidx.databinding.BaseObservable;


/**
 * Created by MMIIT on 2/26/2020.
 */
public class SpeechRecognizer extends BaseObservable implements RecognitionListener {
    android.speech.SpeechRecognizer mRecognizer;
    private static final int RECOGNIZER_TIME = 2000;
    private OnWordRecognizedListener mListener;
    private SpeechRecognizerDialog mDialog;
    private OnErrorListener mErrorListener;
    private Boolean isDone = false;

    public void setOnWordRecognized(SpeechRecognizer.OnWordRecognizedListener listener) {
        this.mListener = listener;
    }

    public void setOnErrorListener(OnErrorListener listener) {
        this.mErrorListener = listener;
    }

    public void start() {
        mRecognizer = android.speech.SpeechRecognizer.createSpeechRecognizer(ApplicationLifecycleService.getCurrentActivity());
        mRecognizer.setRecognitionListener(this);
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        String locale = Database.account().readSpeechLocale();
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, "voice.recognition.test");
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, locale);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, locale);
        intent.putExtra(RecognizerIntent.EXTRA_ONLY_RETURN_LANGUAGE_PREFERENCE, locale);

        mDialog = new SpeechRecognizerDialog(ApplicationLifecycleService.getCurrentActivity(), locale).build();
        mDialog.show();
        mDialog.setOnRefreshListener(() -> mRecognizer.startListening(intent));
        mDialog.setOnCancelListener(() -> mRecognizer.stopListening());
        mRecognizer.startListening(intent);

    }

    public void stop() {
        if (mRecognizer != null) {
            mRecognizer.stopListening();
        }
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }

    @Override
    public void onReadyForSpeech(Bundle bundle) {

    }

    @Override
    public void onBeginningOfSpeech() {
        new Handler().postDelayed(() -> mRecognizer.stopListening(), RECOGNIZER_TIME);
    }

    @Override
    public void onRmsChanged(float v) {

    }

    @Override
    public void onBufferReceived(byte[] bytes) {
        Alerts.info(String.valueOf(bytes));
    }

    @Override
    public void onEndOfSpeech() {
    }

    @Override
    public void onError(int i) {
        mDialog.showError(parseError(i));
        if (mErrorListener != null && i != 5) {
            mErrorListener.onError();
        }
    }

    @Override
    public void onResults(Bundle bundle) {
        if (!isDone) {
            List<String> res = bundle.getStringArrayList(android.speech.SpeechRecognizer.RESULTS_RECOGNITION);
            this.mListener.wordRecognized(res.get(0));

            stop();
            isDone = true;
        }
    }

    @Override
    public void onPartialResults(Bundle bundle) {
    }

    @Override
    public void onEvent(int i, Bundle bundle) {
    }

    private String parseError(int i) {
        switch (i) {
            case 6:
                return ApplicationLifecycleService.getCurrentActivity().getString(R.string.error_speech_no_speech_input);
            case 4:
                return ApplicationLifecycleService.getCurrentActivity().getString(R.string.error_speech_server_error);
            case 8:
                return ApplicationLifecycleService.getCurrentActivity().getString(R.string.error_speech_recognition_busy);
            case 7:
                return ApplicationLifecycleService.getCurrentActivity().getString(R.string.error_speech_no_match);
            case 1:
                return ApplicationLifecycleService.getCurrentActivity().getString(R.string.error_speech_network_error);
            case 2:
                return ApplicationLifecycleService.getCurrentActivity().getString(R.string.error_speech_other_network_error);
            case 9:
                return ApplicationLifecycleService.getCurrentActivity().getString(R.string.error_speech_insufficient_permission);
            case 5:
                return ApplicationLifecycleService.getCurrentActivity().getString(R.string.error_speech_other_client_side);
            case 3:
                return ApplicationLifecycleService.getCurrentActivity().getString(R.string.error_speech_recording_error);
        }
        return "";
    }

    public interface OnWordRecognizedListener {
        void wordRecognized(String text);
    }

    public interface OnErrorListener {
        void onError();
    }
}
