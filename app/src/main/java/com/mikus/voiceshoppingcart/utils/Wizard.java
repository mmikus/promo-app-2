package com.mikus.voiceshoppingcart.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mikus.voiceshoppingcart.data.Database;

/**
 * Created by MMIIT on 5/30/2020.
 */
public class Wizard {
    public static final int DASHBOARD_STEP = 1;
    public static final int ADD_ITEM_TO_LIST_STEP = 2;
    public static final int SWIPE_BASKET_ITEM_STEP = 3;
    public static final int BASKET_ITEM_HELP_STEP = 4;
    public static final int SUMMARIZATION_STEP = 5;
    public static final int DRAWER_MENU_STEP = 6;
    public static final int DONE = 7;
    private static final int SHAKE_ANIMATION_DELAY = 2000;
    private static final int VISIBILITY_DURATION = 500;


    private static ViewGroup mLayout;
    private static ObjectAnimator mAnim;

    public static void run(int step, LinearLayout layout) {
        if (Database.account().getWizartStep() < step) {
            mLayout = layout;
            Animations.visibility(layout, View.VISIBLE, VISIBILITY_DURATION);
        }
    }

    public static void run(int step, RelativeLayout layout) {
        if (Database.account().getWizartStep() < step) {
            mLayout = layout;
            Animations.visibility(layout, View.VISIBLE, VISIBILITY_DURATION);
        }
    }

    public static void run(int step, RelativeLayout layout, TextView title, LinearLayout aboveHelp, LinearLayout belowHelp, View item) {
        if (Database.account().getWizartStep() < step) {
            mLayout = layout;
            Animations.visibility(layout, View.VISIBLE, VISIBILITY_DURATION);
            title.setVisibility(View.GONE);
            aboveHelp.setVisibility(View.VISIBLE);
            belowHelp.setVisibility(View.VISIBLE);
            item.setX(40f);
        }
    }

    public static void run(int step, RelativeLayout layout, TextView view, int text, int elevation) {
        if (Database.account().getWizartStep() < step) {
            mLayout = layout;
            Animations.visibility(layout, View.VISIBLE, VISIBILITY_DURATION);
            layout.setElevation(elevation * 50);
            view.setText(text);
        }
    }

    public static void run(int step, RelativeLayout layout, View animate, int elevation) {
        if (Database.account().getWizartStep() < step) {
            mLayout = layout;
            mLayout.setElevation(elevation);
            Animations.visibility(layout, View.VISIBLE, VISIBILITY_DURATION);

            mAnim = ObjectAnimator.ofFloat(animate, "x", 40f, 170f, -170f, 40f);
            mAnim.setRepeatCount(1);
            mAnim.setDuration(1000);
            mAnim.start();
            mAnim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    new Handler().postDelayed(() -> {
                        if (mAnim != null) {
                            mAnim.start();
                        }
                    }, SHAKE_ANIMATION_DELAY);
                }
            });
        }
    }

    public static void run(int step, RelativeLayout layout, View shake) {
        if (Database.account().getWizartStep() < step) {
            mLayout = layout;
            Animations.visibility(layout, View.VISIBLE, VISIBILITY_DURATION);
            mAnim = ObjectAnimator.ofFloat(shake, "rotation", 0f, 20f, 0f, -20f, 0f);
            mAnim.setRepeatCount(10);
            mAnim.setDuration(100);
            mAnim.start();
            mAnim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    new Handler().postDelayed(() -> {
                        if (mAnim != null) {
                            mAnim.start();
                        }
                    }, SHAKE_ANIMATION_DELAY);
                }
            });
        }
    }

    public static void hide(int step) {
        Database.account().saveWizardStep(step);
        if (mLayout != null) {
            Animations.visibility(mLayout, View.GONE, VISIBILITY_DURATION);
            mLayout = null;
        }
        if (mAnim != null) {
            mAnim.cancel();
            mAnim.removeAllListeners();
            mAnim = null;
        }


    }

}
