package com.mikus.voiceshoppingcart.utils;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.mikus.voiceshoppingcart.services.CheckConnectionTask;

import java.util.concurrent.ExecutionException;

/**
 * Created by MMIIT on 6/11/2020.
 */

public class ConnectionUtil {

    private IConnectionCheckedListener mListener;

    public ConnectionUtil setConnectionCheckedListener(IConnectionCheckedListener listener) {
        mListener = listener;
        return this;
    }

    public ConnectionUtil check() {
        try {
            Boolean hasConnection = new CheckConnectionTask().execute().get();
            if (hasConnection) {
                mListener.onConnected();
            } else {
                mListener.onDisconnected();
            }
        } catch (ExecutionException | InterruptedException e) {
            mListener.onDisconnected();
        }
        return this;
    }

    public interface IConnectionCheckedListener {
        void onConnected();

        void onDisconnected();
    }
}
