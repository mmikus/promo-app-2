package com.mikus.voiceshoppingcart.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.transition.Slide;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;

import com.mikus.voiceshoppingcart.R;
import com.mikus.voiceshoppingcart.views.model.AbstractViewModel;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by MMIIT on 2/27/2020.
 */
public class Animations {
    private static volatile Context mContext;
    private static volatile List<ClassStack> mBackClazzStack;

    public static void init(Context context) {
        Animations.mContext = context;
    }

    public static void visibility(View view, int type){
        visibility(view, type, 300);
    }
    public static void visibility(View view, int type, int duration) {
        if (type == View.VISIBLE) {
            view.animate().alpha(1).setDuration(duration).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    super.onAnimationStart(animation);
                    view.setVisibility(View.VISIBLE);
                }
            }).start();
        } else {
            view.animate().alpha(0).setDuration(duration).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    view.setVisibility(View.GONE);
                }
            }).start();
        }
    }

    public static void visibility(View view) {
        visibility(view, view.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
    }

    public static void height(View view, int initial, int number) {
        int densityInitial = (int) (initial * mContext.getResources().getDisplayMetrics().density);
        int densityNumber = (int) (number * mContext.getResources().getDisplayMetrics().density);
        float density = mContext.getResources().getDisplayMetrics().density;
        number = ((int) view.getHeight() == densityInitial || view.getHeight() != (int) densityNumber) ? number : initial;
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.height = (int) (number * density);
        view.setLayoutParams(params);
    }

    public static void translationY(View view, float initial, float number) {
        int densityInitial = (int) (initial * mContext.getResources().getDisplayMetrics().density);
        float density = mContext.getResources().getDisplayMetrics().density;
        number = ((int) view.getTranslationY() == densityInitial) ? number : initial;
        view.animate().translationY((int) (number * density)).setDuration(300).start();
    }

    public static void visibility(ViewGroup container, boolean visible, View... components) {
        TransitionManager.beginDelayedTransition(container, new Slide(visible ? Gravity.LEFT : Gravity.LEFT));
        for (View component : components) {
            component.setVisibility(visible ? View.VISIBLE : View.GONE);
        }
    }

    public static void toActivity(Context context, Class clazz) {
        Intent intent = new Intent(context, clazz);
        context.startActivity(intent);
    }

    public static void toActivity(Class clazz) {
        Intent intent = new Intent(mContext, clazz);
        mContext.startActivity(intent);
    }

    public static void toActivity(Class toClazz, Class backClazz) {
        addToBackStack(new ClassStack(backClazz));
        Intent intent = new Intent(mContext, toClazz);
        mContext.startActivity(intent);
    }

    public static void showSubActivity(Class clazz, Class backClazz) {
        addToBackStack(new ClassStack(backClazz));
        Intent intent = new Intent(mContext, clazz);
        Bundle animation = ActivityOptions.makeCustomAnimation(mContext, R.anim.slide_in_left, R.anim.slide_in_right).toBundle();
        mContext.startActivity(intent, animation);
    }

    public static void showSubActivity(Class clazz, Class backClazz, Map extras) {
        addToBackStack(new ClassStack(backClazz, extras));
        Intent intent = new Intent(mContext, clazz);
        Iterator<Map.Entry<String, Object>> set = extras.entrySet().iterator();
        while (set.hasNext()) {
            Map.Entry<String, Object> entry = set.next();
            if (entry.getValue() instanceof Integer) {
                intent.putExtra(entry.getKey(), (Integer) entry.getValue());
            } else if (entry.getValue() instanceof String) {
                intent.putExtra(entry.getKey(), (String) entry.getValue());
            } else if (entry.getValue() instanceof Long) {
                intent.putExtra(entry.getKey(), (Long) entry.getValue());
            } else if (entry.getValue() instanceof Boolean) {
                intent.putExtra(entry.getKey(), (Boolean) entry.getValue());
            } else if (entry.getValue() instanceof Float) {
                intent.putExtra(entry.getKey(), (Float) entry.getValue());
            }
        }
        Bundle animation = ActivityOptions.makeCustomAnimation(mContext, R.anim.slide_in_left, R.anim.slide_in_right).toBundle();
        mContext.startActivity(intent, animation);
    }

    public static void showSubActivity(Class clazz, Class backClazz, Boolean add, Map extras) {
        if (add) {
            addToBackStack(new ClassStack(backClazz, extras));
        }
        Intent intent = new Intent(mContext, clazz);
        Iterator<Map.Entry<String, Object>> set = extras.entrySet().iterator();
        while (set.hasNext()) {
            Map.Entry<String, Object> entry = set.next();
            if (entry.getValue() instanceof Integer) {
                intent.putExtra(entry.getKey(), (Integer) entry.getValue());
            } else if (entry.getValue() instanceof String) {
                intent.putExtra(entry.getKey(), (String) entry.getValue());
            } else if (entry.getValue() instanceof Long) {
                intent.putExtra(entry.getKey(), (Long) entry.getValue());
            } else if (entry.getValue() instanceof Boolean) {
                intent.putExtra(entry.getKey(), (Boolean) entry.getValue());
            } else if (entry.getValue() instanceof Float) {
                intent.putExtra(entry.getKey(), (Float) entry.getValue());
            }
        }
        Bundle animation = ActivityOptions.makeCustomAnimation(mContext, R.anim.slide_in_left, R.anim.slide_in_right).toBundle();
        mContext.startActivity(intent, animation);
    }

    public static void showSubActivity(Class clazz, Class backClazz, AbstractViewModel model) {
        addToBackStack(new ClassStack(backClazz));
        Intent intent = new Intent(mContext, clazz);
        Bundle animation = ActivityOptions.makeCustomAnimation(mContext, R.anim.slide_in_left, R.anim.slide_in_right).toBundle();
        intent.putExtra(model.getModelId(), Parcels.wrap(model));
        mContext.startActivity(intent, animation);
    }

    /*public static void showSubActivity(Class clazz, AbstractViewModel model) {
        Intent intent = new Intent(mContext, clazz);
        Bundle animation = ActivityOptions.makeCustomAnimation(mContext, R.anim.slide_in_left, R.anim.slide_in_right).toBundle();
        intent.putExtra(model.getModelId(), Parcels.wrap(model));
        mContext.startActivity(intent, animation);
    }*/

    public static void backToActivity(Class clazz) {
        Intent intent = new Intent(mContext, clazz);
        Bundle animation = ActivityOptions.makeCustomAnimation(mContext, R.anim.slide_out_right, R.anim.slide_out_left).toBundle();
        mContext.startActivity(intent, animation);
    }

    public static void backTwoSteps() {
        //popFromBackStack();
        if (mBackClazzStack != null && mBackClazzStack.size() > 1) {
            mBackClazzStack.remove(mBackClazzStack.size() - 1);
        }
        backToActivity();
    }

    public static void backToActivity() {
        ClassStack stack = getFromStack();
        if (stack != null) {
            Intent intent = new Intent(mContext, stack.getClazz());
            if (stack.getExtras() != null) {
                Iterator<Map.Entry<String, Object>> set = stack.getExtras().entrySet().iterator();
                while (set.hasNext()) {
                    Map.Entry<String, Object> entry = set.next();
                    if (entry.getValue() instanceof Integer) {
                        intent.putExtra(entry.getKey(), (Integer) entry.getValue());
                    } else if (entry.getValue() instanceof String) {
                        intent.putExtra(entry.getKey(), (String) entry.getValue());
                    } else if (entry.getValue() instanceof Long) {
                        intent.putExtra(entry.getKey(), (Long) entry.getValue());
                    } else if (entry.getValue() instanceof Boolean) {
                        intent.putExtra(entry.getKey(), (Boolean) entry.getValue());
                    } else if (entry.getValue() instanceof Float) {
                        intent.putExtra(entry.getKey(), (Float) entry.getValue());
                    }
                }
            }
            Bundle animation = ActivityOptions.makeCustomAnimation(mContext, R.anim.slide_out_right, R.anim.slide_out_left).toBundle();
            mContext.startActivity(intent, animation);
            popFromBackStack();
        }
    }

    public static void backToActivity(AbstractViewModel model) {
        ClassStack stack = getFromStack();
        if (stack != null) {
            Intent intent = new Intent(mContext, stack.mClazz);
            Bundle animation = ActivityOptions.makeCustomAnimation(mContext, R.anim.slide_out_right, R.anim.slide_out_left).toBundle();
            intent.putExtra(model.getModelId(), Parcels.wrap(model));
            mContext.startActivity(intent, animation);
            popFromBackStack();
        }
    }

    public static void backToActivity(Class clazz, AbstractViewModel model) {
        Intent intent = new Intent(mContext, clazz);
        Bundle animation = ActivityOptions.makeCustomAnimation(mContext, R.anim.slide_out_right, R.anim.slide_out_left).toBundle();
        intent.putExtra(model.getModelId(), Parcels.wrap(model));
        mContext.startActivity(intent, animation);
    }

    private static void addToBackStack(ClassStack clazz) {
        if (mBackClazzStack == null) {
            mBackClazzStack = new ArrayList<>();
        }

        mBackClazzStack.add(clazz);
    }

    private static void popFromBackStack() {
        if (mBackClazzStack != null && mBackClazzStack.size() > 0) {
            mBackClazzStack.remove(mBackClazzStack.size() - 1);
        }
    }

    private static ClassStack getFromStack() {
        if (mBackClazzStack != null && mBackClazzStack.size() > 0) {
            return mBackClazzStack.get(mBackClazzStack.size() - 1);
        }

        return null;
    }

    static class ClassStack {
        private Class mClazz;
        private Map mExtras;

        public ClassStack(Class mClazz) {
            this.mClazz = mClazz;
        }

        public ClassStack(Class clazz, Map extras) {
            this.mClazz = clazz;
            this.mExtras = extras;
        }

        public Class getClazz() {
            return mClazz;
        }

        public void setClazz(Class mClazz) {
            this.mClazz = mClazz;
        }

        public Map getExtras() {
            return mExtras;
        }

        public void setExtras(Map mExtras) {
            this.mExtras = mExtras;
        }
    }
}

