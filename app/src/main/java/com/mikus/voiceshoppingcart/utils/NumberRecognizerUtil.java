package com.mikus.voiceshoppingcart.utils;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by MMIIT on 3/29/2020.
 */
public class NumberRecognizerUtil {

    private static final String DELIMITER = " ";
    private static final int FIRST_NUMBER = 0;
    private static final int SECOND_NUMBER = 1;

    public static Double parsePrice(String text) throws NumberFormatException {
        return parseCurrency(text);
    }

    public static Integer parseQuantity(String text) throws NumberFormatException {
        return parseInteger(text);
    }

    private static Double parseDouble(String text) throws NumberFormatException {
        String[] numbers = text.split(DELIMITER);

        if (numbers.length > 1) {
            String first = null;
            String second = null;

            if (!isNumber(numbers[FIRST_NUMBER])) {
                if (mapper.containsKey(numbers[FIRST_NUMBER])) {
                    first = mapper.get(numbers[FIRST_NUMBER]);
                }
            } else {
                first = numbers[FIRST_NUMBER];
            }

            if (!isNumber(numbers[SECOND_NUMBER])) {
                if (mapper.containsKey(numbers[SECOND_NUMBER])) {
                    second = mapper.get(numbers[SECOND_NUMBER]);
                }
            } else {
                second = numbers[SECOND_NUMBER];
            }

            if (first != null && second != null) {
                return Double.parseDouble(first + "." + second);
            } else if (first != null) {
                return Double.parseDouble(first);
            } else {
                throw new NumberFormatException();
            }
        } else if (numbers.length > 0) {
            if (!isNumber(numbers[FIRST_NUMBER])) {
                if (mapper.containsKey(numbers[FIRST_NUMBER])) {
                    return Double.parseDouble(mapper.get(numbers[FIRST_NUMBER]));
                }
            } else {
                return Double.parseDouble(numbers[FIRST_NUMBER]);
            }
        }
        throw new NumberFormatException();
    }

    private static Integer parseInteger(String text) {
        if (!isNumber(text)) {
            if (mapper.containsKey(text)) {
                return Integer.parseInt(mapper.get(text));
            }
        } else {
            return Integer.parseInt(text);
        }
        throw new NumberFormatException();
    }


    private static String parseFloatNumber(String text) {
        Iterator corupted = corruptedFloats.keySet().iterator();
        while (corupted.hasNext()) {
            String key = (String) corupted.next();
            if (text.contains(key)) {
                text = text.replace(key, corruptedFloats.get(key));
            }
        }
        Iterator iterator = floatDelimiter.keySet().iterator();
        while (iterator.hasNext()) {
            String key = (String) iterator.next();

            if (text.contains(key)) {
                text = text.replace(key, "");
            }
        }
        return text.trim().replaceAll(" +", " ");
    }

    private static Double parseCurrency(String text) throws NumberFormatException {
        text = parseFloatNumber(text);
        String[] numbers = text.split(DELIMITER);

        if (numbers.length > 1) {
            if (currencyMapper.containsKey(numbers[FIRST_NUMBER])) {
                if (currencyMapper.get(numbers[FIRST_NUMBER]).equals("c")) {
                    if (!isNumber(numbers[SECOND_NUMBER])) {
                        if (mapper.containsKey(numbers[SECOND_NUMBER])) {
                            Double num = Double.parseDouble(mapper.get(numbers[SECOND_NUMBER]));
                            return parseDouble(String.valueOf(num / 100));
                        }
                    } else {
                        Double num = Double.parseDouble(numbers[SECOND_NUMBER]);
                        return parseDouble(String.valueOf(num / 100));
                    }
                } else if (currencyMapper.get(numbers[FIRST_NUMBER]).equals("e")) {
                    return parseDouble("1 " + numbers[SECOND_NUMBER]);
                } else {
                    return parseDouble(numbers[SECOND_NUMBER]);
                }
            } else if (currencyMapper.containsKey(numbers[SECOND_NUMBER])) {
                if (currencyMapper.get(numbers[SECOND_NUMBER]).equals("c")) {
                    if (!isNumber(numbers[FIRST_NUMBER])) {
                        if (mapper.containsKey(numbers[FIRST_NUMBER])) {
                            Double num = Double.parseDouble(mapper.get(numbers[FIRST_NUMBER]));
                            return parseDouble(String.valueOf(num / 100));
                        }
                    } else {
                        Double num = Double.parseDouble(numbers[FIRST_NUMBER]);
                        return parseDouble(String.valueOf(num / 100));
                    }
                } else {
                    return parseDouble(numbers[FIRST_NUMBER]);
                }
            }
        }
        return parseDouble(text);
    }

    private static boolean isNumber(String text) {
        if (Nulls.isNull(text)) {
            return false;
        }
        try {
            Integer.parseInt(text);
        } catch (NumberFormatException e) {
            try {
                Double.parseDouble(text);
            } catch (NumberFormatException ex) {
                return false;
            }
        }
        return true;
    }

    private static Map<String, String> corruptedFloats = new HashMap<String, String>() {{
        put("veselá", "2 cela");
        put("0 0 ", "0 cela ");
    }};
    private static Map<String, String> floatDelimiter = new HashMap<String, String>() {{
        put("point", ".");
        put("punto", ".");
        put("i", ".");
        put("cele", ".");
        put("cela", ".");
        put("celé", ".");
        put("celá", ".");
        put("telá", ".");
        put("celých", ".");
    }};
    private static Map<String, String> currencyMapper = new HashMap<String, String>() {{
        put("centy", "c");
        put("centi", "c");
        put("centov", "c");
        put("cent", "c");
        put("eura", "e");
        put("eur", "e");
        put("euro", "e");
        put("Euro", "e");
        put("Eur", "e");
        put("Eura", "e");
    }};
    private static Map<String, String> mapper = new HashMap<String, String>() {{
        put("nula", "0");
        put("jeden", "1");
        put("jedno", "1");
        put("tri", "3");
        put("štyri", "4");
        put("styri", "4");
        put("pat", "5");
        put("päť", "5");
        put("šesť", "6");
        put("sest", "6");
        put("sedem", "7");
        put("osem", "8");
        put("devat", "9");
        put("deväť", "9");
        put("dva", "2");
        put("dve", "2");
    }};
}
