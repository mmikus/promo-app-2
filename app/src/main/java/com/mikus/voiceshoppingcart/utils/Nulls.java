package com.mikus.voiceshoppingcart.utils;

/**
 * Created by MMIIT on 2/25/2020.
 */
public class Nulls {
    public static boolean isNull(String text){
        return text == null || text.isEmpty();
    }

    public static boolean isNull(Double number){
        return number == null;
    }

    public static boolean isNull(Integer number){
        return number == null;
    }
}
