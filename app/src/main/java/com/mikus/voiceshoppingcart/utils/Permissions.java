package com.mikus.voiceshoppingcart.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;

import com.mikus.voiceshoppingcart.services.ApplicationLifecycleService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

/**
 * Created by MMIIT on 2/26/2020.
 */
public class Permissions {
    private static final int REQUEST_PERMISSION_CODE = 1;
    private static Map<String, Boolean> permissions = new HashMap<String, Boolean>() {{
        put(Manifest.permission.RECORD_AUDIO, false);
        put(Manifest.permission.MODIFY_AUDIO_SETTINGS, false);
    }};

    public static boolean hasGrantedAllPermission() {
        Iterator it = permissions.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            if (!(Boolean) pair.getValue()) {
                return false;
            }
        }
        return true;
    }

    public static void request(Activity activity) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            request(
                    activity,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.MODIFY_AUDIO_SETTINGS
            );
        }
    }

    public static void request(Activity activity, String... permissions) {
        List<String> req = new ArrayList<>();
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                    Permissions.permissions.put(permission, false);
                } else {
                    Permissions.permissions.put(permission, true);
                    req.add(permission);
                }
            }
        }
        if (req.size() > 0) {
            ActivityCompat.requestPermissions(activity, req.toArray(new String[req.size()]), REQUEST_PERMISSION_CODE);
        }
    }

    public static boolean hasPermission(Context context, String... permissions) {
        for (String permission : permissions) {
            if (context.checkCallingOrSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                Permissions.request(ApplicationLifecycleService.getCurrentActivity(), permission);
                //showPermissionNotGranted(context);
                return false;
            }
        }
        return true;
    }

    public static boolean hasInternetConnection() {
        Context context = ApplicationLifecycleService.getCurrentActivity();
        final ConnectivityManager connectivityManager = (ConnectivityManager) context.
                getSystemService(Context.CONNECTIVITY_SERVICE);

        final Network network = connectivityManager.getActiveNetwork();
        final NetworkCapabilities capabilities = connectivityManager
                .getNetworkCapabilities(network);

        if ((connectivityManager.getActiveNetworkInfo() != null) && connectivityManager.getActiveNetworkInfo().isConnected()) {
            final String command = "ping -c 1 google.com";
            try {
                return Runtime.getRuntime().exec(command).waitFor() == 0;
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        }
        return false;
      /*  return capabilities != null
                && capabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_VALIDATED);*/
    }

    public static void showPermissionNotGranted(Context context) {
       /* new Handler(Looper.getMainLooper()).post(() -> {
            try {
                new AlertDialog.Builder(context)
                        .setTitle(context.getString(R.string.cr_permissions_header))
                        .setMessage(context.getString(R.string.cr_permissions_body))
                        .setPositiveButton(context.getString(R.string.anc_ok), null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            } catch (Exception e) {
            }
        });*/

    }
}
