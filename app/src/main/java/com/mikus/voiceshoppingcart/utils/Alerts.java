package com.mikus.voiceshoppingcart.utils;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.android.material.snackbar.Snackbar;
import com.mikus.voiceshoppingcart.R;

/**
 * Created by MMIIT on 2/24/2020.
 */
public class Alerts {
    private static Context mContext;
    private static View mView;

    public static void init(Context context) {
        mContext = context;
    }

    public static void init(View view) {
        mView = view;
    }

    public static void error(String message) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                if (mView != null) {
                    try {
                        Snackbar sb = Snackbar.make(mView, message, Snackbar.LENGTH_LONG);
                        sb.getView().setBackgroundColor(mContext.getColor(R.color.colorRed));
                        sb.show();
                    } catch (IllegalArgumentException ex) {
                    }
                }
                //Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    public static void error(int message) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                if (mView != null) {
                    try {
                        Snackbar sb = Snackbar.make(mView, message, Snackbar.LENGTH_LONG);
                        sb.getView().setBackgroundColor(mContext.getColor(R.color.colorRed));
                        sb.show();
                    } catch (IllegalArgumentException ex) {
                    }
                }
                //Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    public static void ok(int message) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                if (mView != null) {
                    Snackbar sb = Snackbar.make(mView, message, Snackbar.LENGTH_LONG);
                    sb.getView().setBackgroundColor(mContext.getColor(R.color.colorGreen));
                    sb.show();
                }
                //Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    public static void ok(String message) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                if (mView != null) {
                    Snackbar sb = Snackbar.make(mView, message, Snackbar.LENGTH_LONG);
                    sb.getView().setBackgroundColor(mContext.getColor(R.color.colorGreen));
                    sb.show();
                }
                //Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    public static void info(String message) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                if (mView != null) {
                    Snackbar.make(mView, message, Snackbar.LENGTH_LONG).show();
                    //Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public static void upload(String message) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                if (mView != null) {
                    Snackbar bar = Snackbar.make(mView, message, Snackbar.LENGTH_INDEFINITE);
                    ViewGroup contentLay = (ViewGroup) bar.getView().findViewById(R.id.snackbar_text).getParent();
                    ProgressBar item = new ProgressBar(mContext);
                    contentLay.addView(item);
                    bar.show();
                }
            }
        });
    }
}
