package com.mikus.voiceshoppingcart;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import com.mikus.voiceshoppingcart.activities.dashboard.DashboardActivity;
import com.mikus.voiceshoppingcart.activities.welcome.WelomeActivity;
import com.mikus.voiceshoppingcart.data.Database;
import com.mikus.voiceshoppingcart.data.DatabaseUpdate;
import com.mikus.voiceshoppingcart.services.ApplicationLifecycleService;
import com.mikus.voiceshoppingcart.utils.Animations;

import java.io.FileNotFoundException;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by MMIIT on 2/23/2020.
 */
public class Application extends android.app.Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        try {
            DatabaseUpdate update = new DatabaseUpdate(this);
            ApplicationInfo ai = getPackageManager().getApplicationInfo(getPackageName(), PackageManager.GET_META_DATA);
            RealmConfiguration config = new RealmConfiguration.Builder()
                    .name("vsc.realm")
                    .schemaVersion((int) ai.metaData.get("databaseVersion"))
                    .migration(update)
                    .build();
            Realm.setDefaultConfiguration(config);
            Realm.getDefaultInstance();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        Database.init(this);
        registerActivityLifecycleCallbacks(new ApplicationLifecycleService());
    }
}
