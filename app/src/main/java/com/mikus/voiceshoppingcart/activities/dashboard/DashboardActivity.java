package com.mikus.voiceshoppingcart.activities.dashboard;

import android.os.Bundle;

import com.mikus.voiceshoppingcart.R;
import com.mikus.voiceshoppingcart.activities.AbstractDrawerActivity;
import com.mikus.voiceshoppingcart.data.Database;
import com.mikus.voiceshoppingcart.data.dto.PositionDto;
import com.mikus.voiceshoppingcart.databinding.LayoutDashboardBinding;
import com.mikus.voiceshoppingcart.services.ApplicationLifecycleService;
import com.mikus.voiceshoppingcart.utils.Wizard;
import com.mikus.voiceshoppingcart.views.model.AbstractViewModel;
import com.mikus.voiceshoppingcart.views.model.DashboardViewModel;

import androidx.databinding.ViewDataBinding;

/**
 * Created by MMIIT on 2/24/2020.
 */
public class DashboardActivity extends AbstractDrawerActivity {

    private LayoutDashboardBinding mBinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Database.list().generateTestData();
        PositionDto position = Database.account().readDashBoardBtnPosition();
        mBinder.dAddBtn.initPosition(position.getX(), position.getY())
                .setOnClickListener(() -> ((DashboardViewModel) mModel).onAddNewList(
                        ApplicationLifecycleService.getCurrentActivity()
                )).setOnSavePositionListener((x, y) -> Database.account().saveDashBoardBtnPosition(x, y));

        Wizard.run(Wizard.DASHBOARD_STEP, mBinder.wizardLayout, mBinder.dAddBtn);
    }

    @Override
    public int getLayoutId() {
        return R.layout.layout_dashboard;
    }

    @Override
    public AbstractViewModel getModel() {
        return new DashboardViewModel(getLayoutId(), this);
    }

    @Override
    public void setDataBinder(ViewDataBinding binder) {
        this.mBinder = ((LayoutDashboardBinding) binder);
    }
}
