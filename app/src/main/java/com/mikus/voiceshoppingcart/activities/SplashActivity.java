package com.mikus.voiceshoppingcart.activities;

import android.os.Bundle;
import android.provider.ContactsContract;

import com.mikus.voiceshoppingcart.R;
import com.mikus.voiceshoppingcart.activities.dashboard.DashboardActivity;
import com.mikus.voiceshoppingcart.activities.welcome.WelomeActivity;
import com.mikus.voiceshoppingcart.data.Database;
import com.mikus.voiceshoppingcart.databinding.LayoutSplashScreenBinding;
import com.mikus.voiceshoppingcart.utils.Animations;
import com.mikus.voiceshoppingcart.views.model.AbstractViewModel;
import com.mikus.voiceshoppingcart.views.model.DashboardViewModel;

import java.io.Console;

import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;
import io.realm.Realm;
import io.realm.exceptions.RealmMigrationNeededException;

/**
 * Created by MMIIT on 2/28/2020.
 */
public class SplashActivity extends AbstractCompactActivity {

    private LayoutSplashScreenBinding mBinder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            if (Database.account().hasAccount()) {
                Animations.toActivity(this, DashboardActivity.class);
            } else {
                Animations.toActivity(this, WelomeActivity.class);
            }
        } catch (RealmMigrationNeededException e) {
        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.layout_splash_screen;
    }

    @Override
    public AbstractViewModel getModel() {
        return new DashboardViewModel(getLayoutId());
    }

    @Override
    public void setDataBinder(ViewDataBinding binder) {
        mBinder = (LayoutSplashScreenBinding) binder;
    }
}
