package com.mikus.voiceshoppingcart.activities.welcome;

import com.mikus.voiceshoppingcart.R;
import com.mikus.voiceshoppingcart.activities.AbstractCompactActivity;
import com.mikus.voiceshoppingcart.activities.AbstractMainActivity;
import com.mikus.voiceshoppingcart.databinding.LayoutWelcomeBinding;
import com.mikus.voiceshoppingcart.views.model.AbstractViewModel;
import com.mikus.voiceshoppingcart.views.model.DashboardViewModel;

import androidx.databinding.ViewDataBinding;

/**
 * Created by MMIIT on 2/23/2020.
 */
public class WelomeActivity extends AbstractMainActivity {

    private LayoutWelcomeBinding mBinder;

    @Override
    public int getLayoutId() {
        return R.layout.layout_welcome;
    }

    @Override
    public AbstractViewModel getModel() {
        return new DashboardViewModel(getLayoutId());
    }

    @Override
    public void setDataBinder(ViewDataBinding binder) {
        this.mBinder = ((LayoutWelcomeBinding) binder);
    }
}
