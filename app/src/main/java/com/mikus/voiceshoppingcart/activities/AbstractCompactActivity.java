package com.mikus.voiceshoppingcart.activities;

import android.Manifest;
import android.os.Bundle;
import android.view.KeyEvent;

import org.parceler.Parcels;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.RequestConfiguration;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.mikus.voiceshoppingcart.R;
import com.mikus.voiceshoppingcart.data.Database;
import com.mikus.voiceshoppingcart.utils.Alerts;
import com.mikus.voiceshoppingcart.utils.Animations;
import com.mikus.voiceshoppingcart.utils.Permissions;
import com.mikus.voiceshoppingcart.utils.Wizard;
import com.mikus.voiceshoppingcart.views.model.AbstractViewModel;
import com.mikus.voiceshoppingcart.BR;

import java.util.Arrays;
import java.util.List;

/**
 * Created by MMIIT on 2/23/2020.
 */
public abstract class AbstractCompactActivity extends AppCompatActivity {

    public abstract int getLayoutId();

    public abstract AbstractViewModel getModel();

    public abstract void setDataBinder(ViewDataBinding binder);

    public AbstractViewModel mModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Permissions.request(this);
        Permissions.request(this, Manifest.permission.RECORD_AUDIO);
        Animations.init(this);
        Alerts.init(this);
        if (getIntent().getExtras() != null && !getIntent().getExtras().isEmpty()) {
            String modelId = this.getModel().getModelId();
            mModel = Parcels.unwrap(getIntent().getExtras().getParcelable(modelId));
            getIntent().getExtras().remove(modelId);

        }
        if (mModel == null) {
            mModel = this.getModel();
        }
        ViewDataBinding binding = DataBindingUtil.setContentView(this, this.getLayoutId(), mModel);
        binding.setVariable(BR.model, mModel);
        mModel.setBinder(binding);

        Alerts.init(findViewById(R.id.constraint_layout));
        this.setDataBinder(binding);

        List<String> testDeviceIds = Arrays.asList("test");
        RequestConfiguration configuration =
                new RequestConfiguration.Builder().setTestDeviceIds(testDeviceIds).build();
        MobileAds.setRequestConfiguration(configuration);

        MobileAds.initialize(this, initializationStatus -> {
        });

        AdView mAdView = findViewById(R.id.ad);
        if (mAdView != null) {
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Animations.backToActivity();
    }
}
