package com.mikus.voiceshoppingcart.activities;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.navigation.NavigationView;
import com.mikus.voiceshoppingcart.R;
import com.mikus.voiceshoppingcart.activities.dashboard.DashboardActivity;
import com.mikus.voiceshoppingcart.activities.list.ShoppingListActivity;
import com.mikus.voiceshoppingcart.activities.stat.StatActivity;
import com.mikus.voiceshoppingcart.data.Database;
import com.mikus.voiceshoppingcart.dialogs.EditBudgetDialog;
import com.mikus.voiceshoppingcart.dialogs.LocaleChangeDialog;
import com.mikus.voiceshoppingcart.services.ApplicationLifecycleService;
import com.mikus.voiceshoppingcart.utils.Animations;
import com.mikus.voiceshoppingcart.utils.Wizard;
import com.mikus.voiceshoppingcart.views.model.DashboardViewModel;
import com.mikus.voiceshoppingcart.views.model.ShoppingListViewModel;

import java.util.Arrays;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;


/**
 * Created by MMIIT on 8/20/2018.
 */
public abstract class AbstractDrawerActivity extends AbstractCompactActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NavigationView navigation = findViewById(R.id.drawer);
        navigation.setNavigationItemSelectedListener(this);
        ((DrawerLayout) findViewById(R.id.drawer_layout)).addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                if (Database.account().getWizartStep() == Wizard.SUMMARIZATION_STEP) {
                    Wizard.hide(Wizard.DONE);
                }
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                ((DrawerLayout) findViewById(R.id.drawer_layout)).openDrawer(GravityCompat.START);
                return true;
        }
        return false;
    }

    public void showWizard(View view) {
        new AlertDialog.Builder(ApplicationLifecycleService.getCurrentActivity(), R.style.AlertDialogStyle)
                .setTitle(R.string.rwd_title)
                .setMessage(R.string.rwd_message).setPositiveButton(R.string.ok, (dialogInterface, i) -> {
            dialogInterface.dismiss();
            Database.account().refreshWizard();
            ((DrawerLayout) findViewById(R.id.drawer_layout)).closeDrawer(GravityCompat.START);
            Animations.showSubActivity(DashboardActivity.class, this.getClass());
        }).setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss())
                .show();
    }

    public void showHome(View view) {
        try {
            if (Class.forName(getPackageName() + "." + getLocalClassName()) != DashboardActivity.class) {
                Animations.showSubActivity(DashboardActivity.class, this.getClass());
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        ((DrawerLayout) findViewById(R.id.drawer_layout)).closeDrawer(GravityCompat.START);
    }

    public void showStat(View view) {
        try {
            if (Class.forName(getPackageName() + "." + getLocalClassName()) != StatActivity.class) {
                Animations.showSubActivity(StatActivity.class, this.getClass());
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        ((DrawerLayout) findViewById(R.id.drawer_layout)).closeDrawer(GravityCompat.START);
    }

    public void showBudget(View view) {
        try {
            if (Class.forName(getPackageName() + "." + getLocalClassName()) == DashboardActivity.class) {
                ((DashboardViewModel) mModel).onBudgetEditClicked();
            } else if (Class.forName(getPackageName() + "." + getLocalClassName()) == ShoppingListActivity.class) {
                ((ShoppingListViewModel) mModel).onBudgetEditClicked();
            } else {
                Double mbudget = Database.account().readBudget();
                final EditBudgetDialog dialog = new EditBudgetDialog(ApplicationLifecycleService.getCurrentActivity(), mbudget).build();
                dialog.show();
                dialog.onCancel(() -> dialog.dismiss());
                dialog.onOk((id, budget) -> {
                    dialog.dismiss();
                    Database.account().updateBudget(budget);
                });
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void showLanguage(View view) {
        Map<String, Locale> locales = Arrays.asList(Locale.getAvailableLocales()).stream()
                .collect(Collectors.toMap(Locale::toLanguageTag, l -> l, (text1, text2) -> text1));
        Locale locale = locales.getOrDefault(Database.account().readSpeechLocale(), Locale.getDefault());
        final LocaleChangeDialog dialog = new LocaleChangeDialog(ApplicationLifecycleService.getCurrentActivity(), locale.getDisplayLanguage()).build();
        dialog.show();
        dialog.onCancel(() -> dialog.dismiss());
        dialog.onOk((text) -> {
            dialog.dismiss();
            Database.account().updateLocale(text);
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        try {
            switch (item.getItemId()) {
                case R.id.nav_home:
                    if (Class.forName(getPackageName() + "." + getLocalClassName()) != DashboardActivity.class) {
                        Animations.showSubActivity(DashboardActivity.class, this.getClass());
                    }
                    break;
                case R.id.nav_stat:
                    if (Class.forName(getPackageName() + "." + getLocalClassName()) != StatActivity.class) {
                        Animations.showSubActivity(StatActivity.class, this.getClass());
                    }
                    break;
                default:
                    ((DrawerLayout) findViewById(R.id.drawer_layout)).closeDrawer(GravityCompat.START);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        ((DrawerLayout) findViewById(R.id.drawer_layout)).closeDrawer(GravityCompat.START);
        return false;
    }
}
