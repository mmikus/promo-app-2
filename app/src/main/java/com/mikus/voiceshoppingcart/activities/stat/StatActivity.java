package com.mikus.voiceshoppingcart.activities.stat;

import android.os.Bundle;

import com.mikus.voiceshoppingcart.R;
import com.mikus.voiceshoppingcart.activities.AbstractDrawerActivity;
import com.mikus.voiceshoppingcart.data.Database;
import com.mikus.voiceshoppingcart.data.dto.PositionDto;
import com.mikus.voiceshoppingcart.databinding.LayoutStatisticsBinding;
import com.mikus.voiceshoppingcart.services.ApplicationLifecycleService;
import com.mikus.voiceshoppingcart.views.model.AbstractViewModel;
import com.mikus.voiceshoppingcart.views.model.StatViewModel;

import androidx.databinding.ViewDataBinding;

/**
 * Created by MMIIT on 4/15/2020.
 */
public class StatActivity extends AbstractDrawerActivity {


    private LayoutStatisticsBinding mBinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PositionDto position = Database.account().readStatBtnPosition();
        mBinder.dAddBtn.initPosition(position.getX(), position.getY())
                .setOnClickListener(() -> ((StatViewModel) mModel).onAddNewList(ApplicationLifecycleService.getCurrentActivity()))
                .setOnSavePositionListener((x, y) -> Database.account().saveStatPosition(x, y));
    }

    @Override
    public int getLayoutId() {
        return R.layout.layout_statistics;
    }

    @Override
    public AbstractViewModel getModel() {
        return new StatViewModel(this);
    }


    @Override
    public void setDataBinder(ViewDataBinding binder) {
        this.mBinder = (LayoutStatisticsBinding) binder;
    }


}
