package com.mikus.voiceshoppingcart.activities.list;

import android.os.Bundle;

import com.mikus.voiceshoppingcart.R;
import com.mikus.voiceshoppingcart.activities.AbstractDrawerActivity;
import com.mikus.voiceshoppingcart.activities.dashboard.DashboardActivity;
import com.mikus.voiceshoppingcart.data.Database;
import com.mikus.voiceshoppingcart.data.dto.PositionDto;
import com.mikus.voiceshoppingcart.databinding.LayoutShoppingListBinding;
import com.mikus.voiceshoppingcart.utils.Animations;
import com.mikus.voiceshoppingcart.utils.Wizard;
import com.mikus.voiceshoppingcart.views.model.AbstractViewModel;
import com.mikus.voiceshoppingcart.views.model.ShoppingListViewModel;

import androidx.databinding.ViewDataBinding;

/**
 * Created by MMIIT on 2/25/2020.
 */
public class ShoppingListActivity extends AbstractDrawerActivity {

    private LayoutShoppingListBinding mBinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PositionDto position = Database.account().readListBtnPosition();
        mBinder.lslAddBtn.initPosition(position.getX(), position.getY())
                .hasRecorder()
                .setOnClickListener(() -> ((ShoppingListViewModel) mModel).onAddItem())
                .setOnSavePositionListener((x, y) -> Database.account().saveListBtnPosition(x, y));

        Wizard.run(Wizard.ADD_ITEM_TO_LIST_STEP, mBinder.wizardLayout, mBinder.lslAddBtn);
    }

    @Override
    public int getLayoutId() {
        return R.layout.layout_shopping_list;
    }

    @Override
    public AbstractViewModel getModel() {
        if (getIntent() != null && getIntent().getExtras() != null) {
            int roomId = getIntent().getExtras().getInt("list_id", 0);
            return new ShoppingListViewModel(getLayoutId(), roomId, this);
        } else {
            Animations.toActivity(DashboardActivity.class);
        }

        return null;
    }

    @Override
    public void setDataBinder(ViewDataBinding binder) {
        this.mBinder = ((LayoutShoppingListBinding) binder);
    }
}
