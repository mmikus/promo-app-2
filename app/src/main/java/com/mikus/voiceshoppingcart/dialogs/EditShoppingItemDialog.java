package com.mikus.voiceshoppingcart.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.mikus.voiceshoppingcart.R;
import com.mikus.voiceshoppingcart.databinding.DialogEditShoppingItemBinding;
import com.mikus.voiceshoppingcart.utils.ViewUtil;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;

/**
 * Created by MMIIT on 4/11/2020.
 */
public class EditShoppingItemDialog extends BaseObservable {

    private DialogEditShoppingItemBinding mBinder;
    private Dialog mDialog;
    private int mId;
    private String mTitle;
    private int mQuantity;
    private double mPrice;
    private Context mContext;

    private IOnOkClickListener okClickListener;
    private IOnCancelClickListener onCancelClickListener;

    public EditShoppingItemDialog(Context context, int id, String mTitle, int mQuantity, double mPrice) {
        this.mId = id;
        this.mTitle = mTitle;
        this.mQuantity = mQuantity;
        this.mPrice = mPrice;
        this.mContext = context;
    }

    @Bindable
    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    @Bindable
    public String getQuantity() {
        return mQuantity == 0 ? "" : String.valueOf(mQuantity);
    }

    public void setQuantity(String quantity) {
        this.mQuantity = Integer.valueOf(quantity);
    }

    @Bindable
    public String getPrice() {
        return mQuantity == 0 ? "" : String.valueOf(mPrice / mQuantity);
    }

    public void setPrice(String price) {
        this.mPrice = Double.valueOf(price);
    }


    public EditShoppingItemDialog build() {
        mDialog = new Dialog(mContext);
        mBinder = DataBindingUtil.inflate(LayoutInflater.from(mContext),
                R.layout.dialog_edit_shopping_item, null, false);
        mDialog.setContentView(mBinder.getRoot());
        mDialog.getWindow().setLayout(
                ViewUtil.getWindowWidth(mContext, 90),
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);

        mBinder.eidTitle.setText(getTitle());
        mBinder.eidPrice.setText(getPrice());
        mBinder.eidQuantity.setText(getQuantity());
        return this;
    }

    public EditShoppingItemDialog onOk(IOnOkClickListener listener) {
        okClickListener = listener;
        mBinder.eidOk.setOnClickListener(view -> okClickListener.onOkClicked(mId,
                mBinder.eidTitle.getText().toString(),
                Integer.valueOf(mBinder.eidQuantity.getText().toString()),
                Double.valueOf(mBinder.eidPrice.getText().toString())));
        return this;
    }

    public EditShoppingItemDialog onCancel(IOnCancelClickListener listener) {
        onCancelClickListener = listener;
        mBinder.eidCancel.setOnClickListener(view -> onCancelClickListener.onCancelClicked());

        return this;
    }

    public void show() {
        mDialog.show();
    }

    public void dismiss() {
        mDialog.dismiss();
    }

    public interface IOnOkClickListener {
        void onOkClicked(int id, String title, int quantity, double price);
    }

    public interface IOnCancelClickListener {
        void onCancelClicked();
    }
}
