package com.mikus.voiceshoppingcart.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.mikus.voiceshoppingcart.R;
import com.mikus.voiceshoppingcart.databinding.DialogEditNameLayoutBinding;
import com.mikus.voiceshoppingcart.utils.ViewUtil;

import androidx.databinding.BaseObservable;
import androidx.databinding.DataBindingUtil;

/**
 * Created by MMIIT on 6/4/2020.
 */
public class EditNameDialog extends BaseObservable {

    private DialogEditNameLayoutBinding mBinder;
    private Dialog mDialog;
    private String mName;
    private Context mContext;

    private IOnOkClickListener okClickListener;
    private IOnCancelClickListener onCancelClickListener;

    public EditNameDialog(Context context, String name) {
        this.mName = name;
        this.mContext = context;
    }

    public EditNameDialog build() {
        mDialog = new Dialog(mContext);
        mBinder = DataBindingUtil.inflate(LayoutInflater.from(mContext),
                R.layout.dialog_edit_name_layout, null, false);
        mDialog.setContentView(mBinder.getRoot());
        mDialog.getWindow().setLayout(
                ViewUtil.getWindowWidth(mContext, 90),
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);

        mBinder.enName.setText(mName);

        return this;
    }

    public EditNameDialog onOk(IOnOkClickListener listener) {
        okClickListener = listener;
        mBinder.ebOk.setOnClickListener(view -> okClickListener.onOkClicked(mBinder.enName.getText().toString()));
        return this;
    }

    public EditNameDialog onCancel(IOnCancelClickListener listener) {
        onCancelClickListener = listener;
        mBinder.ebCancel.setOnClickListener(view -> onCancelClickListener.onCancelClicked());

        return this;
    }

    public void show() {
        try {
            mDialog.show();
        } catch (WindowManager.BadTokenException ex) {

        }
    }

    public void dismiss() {
        mDialog.dismiss();
    }

    public interface IOnOkClickListener {
        void onOkClicked(String name);
    }

    public interface IOnCancelClickListener {
        void onCancelClicked();
    }
}
