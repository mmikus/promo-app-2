package com.mikus.voiceshoppingcart.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.mikus.voiceshoppingcart.R;
import com.mikus.voiceshoppingcart.databinding.DialogEditBudgetBinding;
import com.mikus.voiceshoppingcart.databinding.DialogEditShoppingItemBinding;
import com.mikus.voiceshoppingcart.utils.ViewUtil;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;

/**
 * Created by MMIIT on 4/12/2020.
 */
public class EditBudgetDialog extends BaseObservable {

    private DialogEditBudgetBinding mBinder;
    private Dialog mDialog;
    private int mId;
    private Double mBudget;
    private Context mContext;

    private IOnOkClickListener okClickListener;
    private IOnCancelClickListener onCancelClickListener;

    public EditBudgetDialog(Context context, double budget) {
        this.mId = 0;
        this.mBudget = budget;
        this.mContext = context;
    }

    public EditBudgetDialog(Context context, int id, double budget) {
        this.mId = id;
        this.mBudget = budget;
        this.mContext = context;
    }

    public EditBudgetDialog build() {
        mDialog = new Dialog(mContext);
        mBinder = DataBindingUtil.inflate(LayoutInflater.from(mContext),
                R.layout.dialog_edit_budget, null, false);
        mDialog.setContentView(mBinder.getRoot());
        mDialog.getWindow().setLayout(
                ViewUtil.getWindowWidth(mContext, 90),
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);

        mBinder.ebBudget.setText(mBudget.toString());

        return this;
    }

    public EditBudgetDialog onOk(IOnOkClickListener listener) {
        okClickListener = listener;
        mBinder.ebOk.setOnClickListener(view -> okClickListener.onOkClicked(
                mId,
                Double.valueOf(mBinder.ebBudget.getText().toString())));
        return this;
    }

    public EditBudgetDialog onCancel(IOnCancelClickListener listener) {
        onCancelClickListener = listener;
        mBinder.ebCancel.setOnClickListener(view -> onCancelClickListener.onCancelClicked());

        return this;
    }

    public void show() {
        try {
            mDialog.show();
        } catch (WindowManager.BadTokenException ex) {

        }
    }

    public void dismiss() {
        mDialog.dismiss();
    }

    public interface IOnOkClickListener {
        void onOkClicked(int id, double budget);
    }

    public interface IOnCancelClickListener {
        void onCancelClicked();
    }
}
