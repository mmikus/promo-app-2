package com.mikus.voiceshoppingcart.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.mikus.voiceshoppingcart.R;
import com.mikus.voiceshoppingcart.databinding.DialogAddNewListBinding;
import com.mikus.voiceshoppingcart.databinding.DialogListNoteBinding;
import com.mikus.voiceshoppingcart.utils.Nulls;
import com.mikus.voiceshoppingcart.utils.ViewUtil;

import androidx.databinding.BaseObservable;
import androidx.databinding.DataBindingUtil;

/**
 * Created by MMIIT on 4/14/2020.
 */
public class AddNewListDialog extends BaseObservable {

    private DialogAddNewListBinding mBinder;
    private Dialog mDialog;
    private String mNote;
    private Double mBudget;
    private Context mContext;

    private IOnOkClickListener okClickListener;
    private IOnCancelClickListener onCancelClickListener;

    public AddNewListDialog(Context context) {
        this.mContext = context;
        this.mNote = null;
    }

    public AddNewListDialog(Context context, String note, Double budget) {
        this.mNote = note;
        this.mBudget = budget;
        this.mContext = context;
    }

    public AddNewListDialog build() {
        mDialog = new Dialog(mContext);
        mBinder = DataBindingUtil.inflate(LayoutInflater.from(mContext),
                R.layout.dialog_add_new_list, null, false);
        mDialog.setContentView(mBinder.getRoot());
        mDialog.getWindow().setLayout(
                ViewUtil.getWindowWidth(mContext, 90),
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);

        mBinder.nlNote.setText(mNote);

        return this;
    }

    public AddNewListDialog onOk(IOnOkClickListener listener) {
        okClickListener = listener;
        mBinder.ebOk.setOnClickListener(view -> okClickListener.onOkClicked(
                mBinder.nlNote.getText().toString(),
                Nulls.isNull(mBinder.nlBudget.getText().toString()) ? 0d :
                        Double.valueOf(mBinder.nlBudget.getText().toString())));
        return this;
    }

    public AddNewListDialog onCancel(IOnCancelClickListener listener) {
        onCancelClickListener = listener;
        mBinder.ebCancel.setOnClickListener(view -> onCancelClickListener.onCancelClicked());

        return this;
    }

    public void show() {
        try {
            mDialog.show();
        } catch (WindowManager.BadTokenException ex) {

        }
    }

    public void dismiss() {
        mDialog.dismiss();
    }

    public interface IOnOkClickListener {
        void onOkClicked(String note, double budget);
    }

    public interface IOnCancelClickListener {
        void onCancelClicked();
    }
}
