package com.mikus.voiceshoppingcart.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.mikus.voiceshoppingcart.R;
import com.mikus.voiceshoppingcart.databinding.DialogEditBudgetBinding;
import com.mikus.voiceshoppingcart.databinding.DialogListNoteBinding;
import com.mikus.voiceshoppingcart.utils.ViewUtil;

import androidx.core.content.ContextCompat;
import androidx.databinding.BaseObservable;
import androidx.databinding.DataBindingUtil;

/**
 * Created by MMIIT on 4/12/2020.
 */
public class ListNoteDialog extends BaseObservable {

    private DialogListNoteBinding mBinder;
    private Dialog mDialog;
    private int mId;
    private String mNote;
    private Context mContext;

    private IOnOkClickListener okClickListener;
    private IOnCancelClickListener onCancelClickListener;

    public ListNoteDialog(Context context){
        this.mContext = context;
        this.mId = 0;
        this.mNote = null;
    }
    public ListNoteDialog(Context context, int id, String note) {
        this.mId = id;
        this.mNote = note;
        this.mContext = context;
    }

    public ListNoteDialog build() {
        mDialog = new Dialog(mContext);
        mBinder = DataBindingUtil.inflate(LayoutInflater.from(mContext),
                R.layout.dialog_list_note, null, false);
        mDialog.setContentView(mBinder.getRoot());
        mDialog.getWindow().setLayout(
                ViewUtil.getWindowWidth(mContext, 90),
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);

        mBinder.lnNote.setText(mNote);

        return this;
    }

    public ListNoteDialog onOk(IOnOkClickListener listener) {
        okClickListener = listener;
        mBinder.ebOk.setOnClickListener(view -> okClickListener.onOkClicked(
                mId,
                mBinder.lnNote.getText().toString()));
        return this;
    }

    public ListNoteDialog onCancel(IOnCancelClickListener listener) {
        onCancelClickListener = listener;
        mBinder.ebCancel.setOnClickListener(view -> onCancelClickListener.onCancelClicked());

        return this;
    }

    public void show() {
        try {
            mDialog.show();
        } catch (WindowManager.BadTokenException ex) {

        }
    }

    public void dismiss() {
        mDialog.dismiss();
    }

    public interface IOnOkClickListener {
        void onOkClicked(int id, String note);
    }

    public interface IOnCancelClickListener {
        void onCancelClicked();
    }
}
