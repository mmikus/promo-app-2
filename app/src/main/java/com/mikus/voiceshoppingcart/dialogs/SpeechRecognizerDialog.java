package com.mikus.voiceshoppingcart.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.mikus.voiceshoppingcart.R;
import com.mikus.voiceshoppingcart.databinding.DialogSpeechRecognizerBinding;
import com.mikus.voiceshoppingcart.utils.ViewUtil;

import java.util.Arrays;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import androidx.databinding.DataBindingUtil;

/**
 * Created by MMIIT on 5/8/2020.
 */
public class SpeechRecognizerDialog {

    private static final float PULSAR_SIZE = 1.4f;
    DialogSpeechRecognizerBinding mBinder;
    private Context mContext;
    private String mLocale;
    private Dialog mDialog;
    private Map<String, Locale> mLocales;
    private OnRefreshListener mListener;
    private OnCancelListener mCancelListener;

    private Runnable mPulsarEffect;
    private Handler mHandlerPulsar;

    public SpeechRecognizerDialog(Context context, String locale) {
        this.mContext = context;
        this.mLocale = locale;
    }

    public SpeechRecognizerDialog build() {
        mDialog = new Dialog(mContext);
        mBinder = DataBindingUtil.inflate(LayoutInflater.from(mContext),
                R.layout.dialog_speech_recognizer, null, false);
        mDialog.setContentView(mBinder.getRoot());
        mDialog.getWindow().setLayout(
                ViewUtil.getWindowWidth(mContext, 90),
                LinearLayout.LayoutParams.WRAP_CONTENT
        );

        mLocales = Arrays.asList(Locale.getAvailableLocales()).stream()
                .collect(Collectors.toMap(Locale::toLanguageTag, l -> l, (text1, text2) -> text1));

        String text = mLocales.containsKey(mLocale) ? mLocales.get(mLocale).getDisplayLanguage() : mLocales.get("en").getDisplayLanguage();
        mBinder.dsrLocal.setText(text);

        mPulsarEffect = () -> {
            mBinder.dsrPuls1.animate().scaleX(PULSAR_SIZE).scaleY(PULSAR_SIZE).alpha(0f).setDuration(700).withEndAction(() -> {
                mBinder.dsrPuls1.setScaleX(1f);
                mBinder.dsrPuls1.setScaleY(1f);
                mBinder.dsrPuls1.setAlpha(1f);
            });

            mBinder.dsrPuls2.animate().scaleX(PULSAR_SIZE).scaleY(PULSAR_SIZE).alpha(0f).setDuration(700).withEndAction(() -> {
                mBinder.dsrPuls2.setScaleX(1f);
                mBinder.dsrPuls2.setScaleY(1f);
                mBinder.dsrPuls2.setAlpha(1f);
            });
            mHandlerPulsar.postDelayed(mPulsarEffect, 1500);
        };
        mHandlerPulsar = new Handler();
        mDialog.setOnCancelListener(dialogInterface -> {
            mHandlerPulsar.removeCallbacks(mPulsarEffect);
            mCancelListener.onCancel();
        });
        mPulsarEffect.run();
        return this;
    }

    public void setOnCancelListener(OnCancelListener listener) {
        this.mCancelListener = listener;
    }

    public void setOnRefreshListener(OnRefreshListener listener) {
        this.mListener = listener;
    }

    public void showError(String error) {
        mHandlerPulsar.removeCallbacks(mPulsarEffect);
        mBinder.dsrRecordIcon.setImageDrawable(mContext.getDrawable(R.drawable.replay));
        mBinder.dsrRecordIcon.setBackground(mContext.getDrawable(R.drawable.circle_orange));
        mBinder.dsrRecordIcon.setOnClickListener(view -> {
            mBinder.dsrError.setText(mContext.getText(R.string.srd_speech));
            mBinder.dsrRecordIcon.setImageDrawable(mContext.getDrawable(R.drawable.mic));
            mBinder.dsrRecordIcon.setBackground(mContext.getDrawable(R.drawable.circle_green));
            mListener.onRefresh();
            mPulsarEffect.run();
            mBinder.dsrRecordIcon.setOnClickListener(null);
        });
        mBinder.dsrError.setText(error);
    }

    public void show() {
        try {
            mDialog.show();
        } catch (WindowManager.BadTokenException ex) {

        }
    }

    public void dismiss() {
        mDialog.dismiss();
        mHandlerPulsar.removeCallbacks(mPulsarEffect);
    }

    public interface OnRefreshListener {
        void onRefresh();
    }

    public interface OnCancelListener {
        void onCancel();
    }
}
