package com.mikus.voiceshoppingcart.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;

import com.mikus.voiceshoppingcart.R;
import com.mikus.voiceshoppingcart.data.Database;
import com.mikus.voiceshoppingcart.databinding.DialogLocaleChangeBinding;
import com.mikus.voiceshoppingcart.utils.ViewUtil;

import java.util.Arrays;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import androidx.databinding.BaseObservable;
import androidx.databinding.DataBindingUtil;

/**
 * Created by MMIIT on 5/7/2020.
 */
public class LocaleChangeDialog extends BaseObservable {
    private DialogLocaleChangeBinding mBinder;
    private Dialog mDialog;
    private Context mContext;
    private String mLocale;
    private Map<String, Locale> mLocales;

    private IOnOkClickListener okClickListener;
    private IOnCancelClickListener onCancelClickListener;

    public LocaleChangeDialog(Context context, String locale) {
        this.mContext = context;
        this.mLocale = locale;
    }

    public LocaleChangeDialog build() {
        mDialog = new Dialog(mContext);
        mBinder = DataBindingUtil.inflate(LayoutInflater.from(mContext),
                R.layout.dialog_locale_change, null, false);
        mDialog.setContentView(mBinder.getRoot());
        mDialog.getWindow().setLayout(
                ViewUtil.getWindowWidth(mContext, 90),
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);

        mLocales = Arrays.asList(Locale.getAvailableLocales()).stream()
                .collect(Collectors.toMap(Locale::getDisplayLanguage, l -> l, (text1, text2) -> text1));

        ArrayAdapter<String> localeAdapter = new ArrayAdapter<>(mContext, R.layout.item_autocomplete_layout, R.id.autocomplete_item,
                mLocales.keySet().toArray(new String[mLocales.size()]));

        mBinder.lcLocale.setText(mLocale);
        mBinder.lcLocale.setAdapter(localeAdapter);
        return this;
    }

    public LocaleChangeDialog onOk(IOnOkClickListener listener) {
        okClickListener = listener;

        mBinder.lcOk.setOnClickListener(view -> {
            Locale locale = mLocales.getOrDefault(mBinder.lcLocale.getText().toString(), Locale.getDefault());
            okClickListener.onOkClicked(locale.toLanguageTag());
        });
        return this;
    }

    public LocaleChangeDialog onCancel(IOnCancelClickListener listener) {
        onCancelClickListener = listener;
        mBinder.lcCancel.setOnClickListener(view -> onCancelClickListener.onCancelClicked());

        return this;
    }

    public void show() {
        try {
            mDialog.show();
        } catch (WindowManager.BadTokenException ex) {

        }
    }

    public void dismiss() {
        mDialog.dismiss();
    }

    public interface IOnOkClickListener {
        void onOkClicked(String text);
    }

    public interface IOnCancelClickListener {
        void onCancelClicked();
    }
}
