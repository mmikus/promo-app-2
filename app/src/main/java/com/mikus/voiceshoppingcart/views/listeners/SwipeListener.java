package com.mikus.voiceshoppingcart.views.listeners;

/**
 * Created by MMIIT on 3/29/2020.
 */
public interface SwipeListener {

    void onItemSwipedLeft(int position);

    void onItemSwipedRight(int position);
}
