package com.mikus.voiceshoppingcart.views.listeners;

/**
 * Created by MMIIT on 2/27/2020.
 */
public interface RecognizerListener {
    void onTextRecognized(String text);

    void onErrorRecognized();
}
