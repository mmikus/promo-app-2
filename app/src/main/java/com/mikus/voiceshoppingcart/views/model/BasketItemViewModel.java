package com.mikus.voiceshoppingcart.views.model;

import android.app.AlertDialog;

import com.mikus.voiceshoppingcart.R;
import com.mikus.voiceshoppingcart.activities.stat.StatActivity;
import com.mikus.voiceshoppingcart.data.Database;
import com.mikus.voiceshoppingcart.databinding.ItemLayoutBasketListBindingImpl;
import com.mikus.voiceshoppingcart.dialogs.EditShoppingItemDialog;
import com.mikus.voiceshoppingcart.services.ApplicationLifecycleService;
import com.mikus.voiceshoppingcart.utils.Alerts;
import com.mikus.voiceshoppingcart.utils.ConnectionUtil;
import com.mikus.voiceshoppingcart.utils.Nulls;
import com.mikus.voiceshoppingcart.utils.NumberRecognizerUtil;
import com.mikus.voiceshoppingcart.utils.SpeechRecognizer;

import java.util.Locale;

import androidx.databinding.Bindable;

/**
 * Created by MMIIT on 3/28/2020.
 */
public class BasketItemViewModel extends AbstractViewModel {

    private static final int DEFAULT_TRY_COUNTER = 3;

    private int priceTryCounter = DEFAULT_TRY_COUNTER;
    private int nameTryCounter = DEFAULT_TRY_COUNTER;
    private int quantityTryCounter = DEFAULT_TRY_COUNTER;
    private int mId;
    private int mListId;
    private Boolean mInBasket;
    private String mName;
    private Double mPrice;
    private Integer mQuantity;

    private IBasketItemChangedListener itemChangedListener;

    public BasketItemViewModel() {
    }

    public BasketItemViewModel(int listId, int id, Boolean inBasket, String mName, Double mPrice, Integer mQuantity, IBasketItemChangedListener listener) {
        this.mListId = listId;
        this.mId = id;
        this.mName = mName;
        this.mPrice = mPrice;
        this.mQuantity = mQuantity;
        this.mInBasket = inBasket;
        this.itemChangedListener = listener;
    }

    public void update(String name, double price, int quantity) {
        this.mName = name;
        this.mPrice = price;
        this.mQuantity = quantity;
    }

    @Bindable
    public String getName() {
        return mName;
    }

    @Bindable
    public String getPrice() {
        return Nulls.isNull(mPrice) ? "" :
                String.format(Locale.US, "%.2f", mPrice * mQuantity);
    }

    @Bindable
    public String getQuantity() {
        return Nulls.isNull(mQuantity) ? "" : mQuantity.toString();
    }

    @Bindable
    public boolean isInBasket() {
        return mInBasket;
    }

    @Bindable
    public boolean isTransparentBackground() {
        if (isStatistics()) {
            return true;
        }
        return !mInBasket;
    }

    @Bindable
    public boolean isStatistics() {
        return ApplicationLifecycleService.getCurrentActivity() instanceof StatActivity;
    }

    @Override
    public String getModelId() {
        return BasketItemViewModel.class.getName();
    }

    public int getId() {
        return mId;
    }

    public int getListId() {
        return mListId;
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_layout_basket_list;
    }

    public void onAddToBasket() {
        Database.list().addToBasket(((ItemLayoutBasketListBindingImpl) mBinder).ilbInBasketChck.isChecked(), mId);
        Database.list().updateBillInList(mListId);
        mInBasket = ((ItemLayoutBasketListBindingImpl) mBinder).ilbInBasketChck.isChecked();
        notifyChange();
        itemChangedListener.onItemChanged();
    }

    public void onAddPrice() {
        if (isStatistics() || this.mListId == -1) {
            return;
        }
        new ConnectionUtil().setConnectionCheckedListener(new ConnectionUtil.IConnectionCheckedListener() {
            @Override
            public void onConnected() {
                SpeechRecognizer recognizer = new SpeechRecognizer();
                recognizer.setOnWordRecognized(text -> {
                    try {
                        mPrice = NumberRecognizerUtil.parsePrice(text);
                        updateItem();
                    } catch (NumberFormatException e) {
                        Alerts.error(R.string.error_insert_numeric_value);
                    }
                });
                recognizer.setOnErrorListener(() -> tryCounter(priceTryCounter--));
                recognizer.start();
            }

            @Override
            public void onDisconnected() {
                Alerts.error(R.string.error_no_internet_connection);
                showEditDialog();
            }
        }).check();
    }

    public void onAddQuantity() {
        if (isStatistics() || this.mListId == -1) {
            return;
        }
        new ConnectionUtil().setConnectionCheckedListener(new ConnectionUtil.IConnectionCheckedListener() {
            @Override
            public void onConnected() {
                SpeechRecognizer recognizer = new SpeechRecognizer();
                recognizer.setOnWordRecognized(text -> {
                    try {
                        mQuantity = NumberRecognizerUtil.parseQuantity(text);
                        updateItem();
                    } catch (NumberFormatException e) {
                        Alerts.error(R.string.error_insert_numeric_value);
                    }
                });
                recognizer.setOnErrorListener(() -> tryCounter(quantityTryCounter--));
                recognizer.start();
            }

            @Override
            public void onDisconnected() {
                Alerts.error(R.string.error_no_internet_connection);
                showEditDialog();
            }
        }).check();
    }

    public void onEditText() {
        if (isStatistics() || this.mListId == -1) {
            return;
        }
        new ConnectionUtil().setConnectionCheckedListener(new ConnectionUtil.IConnectionCheckedListener() {
            @Override
            public void onConnected() {
                SpeechRecognizer recognizer = new SpeechRecognizer();
                recognizer.setOnWordRecognized(text -> {
                    mName = text;
                    updateItem();
                });
                recognizer.setOnErrorListener(() -> tryCounter(nameTryCounter--));
                recognizer.start();
            }

            @Override
            public void onDisconnected() {
                Alerts.error(R.string.error_no_internet_connection);
                showEditDialog();
            }
        }).check();
    }

    private void tryCounter(int count) {
        if (count < 0) {
            new AlertDialog.Builder(ApplicationLifecycleService.getCurrentActivity(), R.style.AlertDialogStyle)
                    .setTitle(R.string.oqd_title)
                    .setMessage(R.string.oqd_message).setPositiveButton(R.string.ok, (dialogInterface, i) -> {
                dialogInterface.dismiss();
                showEditDialog();
            }).setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss())
                    .show();
            priceTryCounter = DEFAULT_TRY_COUNTER;
            quantityTryCounter = DEFAULT_TRY_COUNTER;
            nameTryCounter = DEFAULT_TRY_COUNTER;
        }
    }

    private void showEditDialog() {
        final EditShoppingItemDialog dialog = new EditShoppingItemDialog(
                ApplicationLifecycleService.getCurrentActivity(),
                mId,
                mName,
                mQuantity,
                mPrice
        ).build();
        dialog.show();
        dialog.onCancel(() -> {
            dialog.dismiss();
        });
        dialog.onOk((id, title, quantity, price) -> {
            dialog.dismiss();
            updateItem(id, title, price, quantity);
        });
    }

    public void updateItem(int id, String name, double price, int quantity) {
        this.mName = name;
        this.mPrice = price;
        this.mQuantity = quantity;
        this.mInBasket = (price > 0.0);
        Database.list().updateItemInList(id, name, price, quantity);
        Database.list().updateBillInList(mListId);
        notifyChange();
    }

    public void updateItem() {
        updateItem(mId, mName, mPrice, mQuantity);
        itemChangedListener.onItemChanged();
    }

    public interface IBasketItemChangedListener {
        void onItemChanged();
    }
}


