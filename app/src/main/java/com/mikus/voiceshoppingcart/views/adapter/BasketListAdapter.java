package com.mikus.voiceshoppingcart.views.adapter;

import com.mikus.voiceshoppingcart.views.model.BasketItemViewModel;

import androidx.databinding.ObservableArrayList;

/**
 * Created by MMIIT on 3/28/2020.
 */
public class BasketListAdapter extends AbstractRecycleAdapter<BasketItemViewModel> {
    public BasketListAdapter(ObservableArrayList<BasketItemViewModel> model) {
        super(model);
    }
}
