package com.mikus.voiceshoppingcart.views.adapter;

import com.mikus.voiceshoppingcart.views.model.DefaultStatItemViewModel;

import androidx.databinding.ObservableArrayList;

/**
 * Created by MMIIT on 4/15/2020.
 */
public class DefaultListAdapter extends AbstractAdapter<DefaultStatItemViewModel> {

    public DefaultListAdapter(ObservableArrayList<DefaultStatItemViewModel> model) {
        super(model);
    }
}
