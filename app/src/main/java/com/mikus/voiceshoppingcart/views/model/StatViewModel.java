package com.mikus.voiceshoppingcart.views.model;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.mikus.voiceshoppingcart.R;
import com.mikus.voiceshoppingcart.activities.dashboard.DashboardActivity;
import com.mikus.voiceshoppingcart.activities.list.ShoppingListActivity;
import com.mikus.voiceshoppingcart.data.Database;
import com.mikus.voiceshoppingcart.databinding.LayoutStatisticsBinding;
import com.mikus.voiceshoppingcart.dialogs.AddNewListDialog;
import com.mikus.voiceshoppingcart.dialogs.EditShoppingItemDialog;
import com.mikus.voiceshoppingcart.services.ApplicationLifecycleService;
import com.mikus.voiceshoppingcart.utils.Animations;
import com.mikus.voiceshoppingcart.utils.Nulls;
import com.mikus.voiceshoppingcart.utils.PrettyDateUtil;
import com.mikus.voiceshoppingcart.views.adapter.BasketListAdapter;
import com.mikus.voiceshoppingcart.views.adapter.DefaultListAdapter;
import com.mikus.voiceshoppingcart.views.adapter.ShoppingListAdapter;
import com.mikus.voiceshoppingcart.views.model.component.ListBindingComponent;
import com.mikus.voiceshoppingcart.views.model.component.SwipeBindingComponent;
import com.mikus.voiceshoppingcart.views.elements.SwipeItemTouchHelperCallback;

import org.parceler.Transient;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Locale;

import androidx.core.content.ContextCompat;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableArrayList;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by MMIIT on 4/15/2020.
 */
public class StatViewModel extends AbstractViewModel implements ListBindingComponent, SwipeBindingComponent {

    private ObservableArrayList<DefaultStatItemViewModel> mMonthItems;
    private ObservableArrayList<ShoppingItemViewModel> mShoppingListItems;
    private ObservableArrayList<BasketItemViewModel> mBasketItems;

    private RecyclerView.Adapter mShoppingListAdapter;
    private RecyclerView.Adapter mBasketAdapter;


    @Transient
    private Activity mActivity;

    private String mMonth;
    private String mBasketTitle;
    private String mName;
    private String mStatForMonth;

    private Double mSummary;
    private Double mBudget;

    private Double mYearBudget;
    private Double mYearSpents;

    public StatViewModel(Activity activity) {
        this.mActivity = activity;
    }

    @Override
    public String getModelId() {
        return StatViewModel.class.getName();
    }

    @Override
    public int getLayoutId() {
        return R.layout.layout_statistics;
    }

    @Bindable
    public String getName() {
        if (mName == null) {
            mName = Database.account().readName();
            String pattern = mActivity.getString(R.string.sl_name);
            mName = pattern.replace("%name%", mName).replace("%year%", String.valueOf(PrettyDateUtil.getActualYear()));

        }
        return mName;
    }

    @Bindable
    public String getStatForMonth() {
        if (mStatForMonth == null) {
            mStatForMonth = Database.account().readName();
            String pattern = mActivity.getString(R.string.sl_shopping_for_march);
            try {
                mStatForMonth = pattern.replace("%month%", PrettyDateUtil.getActualMonthLocalized());
            } catch (ParseException e) {
                mStatForMonth = pattern.replace("%month%", "January");
            }

        }
        return mStatForMonth;
    }

    @Bindable
    public String getYearBudget() {
        if (mYearBudget == null) {
            mYearBudget = Database.stat().getBudgetByYear();
        }

        return String.format(Locale.US, "%.2f", mYearBudget);
    }

    @Bindable
    public String getYearSpents() {
        if (mYearSpents == null) {
            mYearSpents = Database.stat().getSpentsByYear();
        }

        return String.format(Locale.US, "-%.2f", mYearSpents);
    }

    @Bindable
    public String getYearSummary() {
        return String.format(Locale.US, "%.2f", Double.valueOf(getYearBudget()) + Double.valueOf(getYearSpents()));
    }

    @Bindable
    public boolean isOnBudget(){
        return Double.valueOf(getYearBudget()) + Double.valueOf(getYearSpents()) >= 0;
    }

    @Bindable
    public String getBudget() {
        if (mBudget == null) {
            mBudget = Database.account().readBudget();
        }
        return mBudget.toString();
    }

    @Bindable
    public String getSummary() {
        if (mSummary == null) {
            Double spents = Database.stat().getSpentsByCurrentMonth();
            mSummary = Double.valueOf(getBudget()) - spents;
        }
        return String.format(Locale.US, "%.2f", mSummary);
    }

    @Bindable
    public String getMonth() {
        if (!Nulls.isNull(mMonth)) {
            mMonth = " - " + mMonth;
        }
        return mMonth;
    }

    @Bindable
    public String getBasketTitle() {
        if (!Nulls.isNull(mBasketTitle)) {
            mBasketTitle = " - " + mBasketTitle;
        }
        return mBasketTitle;
    }

    public ObservableArrayList<DefaultStatItemViewModel> listMonthItems() {
        if (mMonthItems == null) {
            mMonthItems = Database.stat().listMonthData();
        }
        return mMonthItems;
    }

    public ObservableArrayList<ShoppingItemViewModel> listShoppingListItems() {
        if (mShoppingListItems == null) {
            mShoppingListItems = new ObservableArrayList<>();
        }

        return mShoppingListItems;
    }

    public ObservableArrayList<BasketItemViewModel> listBasketItems() {
        if (mBasketItems == null) {
            mBasketItems = new ObservableArrayList<>();
        }

        return mBasketItems;
    }


    public void onAddNewList(Context context) {
        final AddNewListDialog dialog = new AddNewListDialog(context).build();
        dialog.show();

        dialog.onCancel(() -> {
            dialog.dismiss();
        });
        dialog.onOk((note, budget) -> {
            dialog.dismiss();
            ShoppingItemViewModel model = Database.list().addNewList(note, budget);
            Animations.showSubActivity(ShoppingListActivity.class, DashboardActivity.class, new HashMap<String, Object>() {{
                put("list_id", model.getBasketId());
            }});
        });
    }

    @Override
    public void bindList(ListView view, Object list, Context context) {
        final ListAdapter adapter = new DefaultListAdapter((ObservableArrayList<DefaultStatItemViewModel>) list);
        view.setAdapter(adapter);

        view.setOnItemClickListener((adapterView, view1, i, l) -> {
            mShoppingListItems.addAll(Database.list().listShoppingCarts(((ObservableArrayList<DefaultStatItemViewModel>) list).get(i).getFrom(),
                    ((ObservableArrayList<DefaultStatItemViewModel>) list).get(i).getTo()));
            fillMonthHeader(((ObservableArrayList<DefaultStatItemViewModel>) list).get(i));
            Animations.visibility(((LayoutStatisticsBinding) mBinder).lsMonthList, View.GONE);
            Animations.visibility(((LayoutStatisticsBinding) mBinder).lsBasketList, View.GONE);
            Animations.visibility(((LayoutStatisticsBinding) mBinder).lsCartItem, View.GONE);
            Animations.visibility(((LayoutStatisticsBinding) mBinder).lsShoppingList, View.VISIBLE);
            Animations.visibility(((LayoutStatisticsBinding) mBinder).lsMonthItem, View.VISIBLE);
            mMonth = ((ObservableArrayList<DefaultStatItemViewModel>) list).get(i).getMonth();
            ((LayoutStatisticsBinding) mBinder).lsMonth.setText(getMonth());
        });
    }

    @Override
    public void setItemSwipeToRecyclerView(RecyclerView recyclerView, boolean swipeEnabled, Drawable drawableSwipeLeft,
                                           Drawable drawableSwipeRight, int bgColorSwipeLeft, int bgColorSwipeRight, Context context) {
        if (recyclerView.getId() == ((LayoutStatisticsBinding) mBinder).lsShoppingList.getId()) {
            bindShoppingList(recyclerView, swipeEnabled, drawableSwipeLeft, drawableSwipeRight, bgColorSwipeLeft, bgColorSwipeRight, context);
        } else if (recyclerView.getId() == ((LayoutStatisticsBinding) mBinder).lsBasketList.getId()) {
            bindBasketList(recyclerView, swipeEnabled, drawableSwipeLeft, drawableSwipeRight, bgColorSwipeLeft, bgColorSwipeRight, context);
        }
    }

    private void bindShoppingList(RecyclerView recyclerView, boolean swipeEnabled, Drawable drawableSwipeLeft,
                                  Drawable drawableSwipeRight, int bgColorSwipeLeft, int bgColorSwipeRight, Context context) {

        mShoppingListAdapter = new ShoppingListAdapter(listShoppingListItems(), model -> {
            mBasketItems.clear();
            mBasketItems.addAll(Database.list().listItems(((ShoppingItemViewModel) model).getId(), true, () -> {
            }));
            Animations.visibility(((LayoutStatisticsBinding) mBinder).lsMonthList, View.GONE);
            Animations.visibility(((LayoutStatisticsBinding) mBinder).lsCartItem, View.VISIBLE);
            Animations.visibility(((LayoutStatisticsBinding) mBinder).lsBasketList, View.VISIBLE);
            Animations.visibility(((LayoutStatisticsBinding) mBinder).lsShoppingList, View.GONE);
            Animations.visibility(((LayoutStatisticsBinding) mBinder).lsMonthItem, View.VISIBLE);
            fillCartHeader((ShoppingItemViewModel) model);
            mBasketTitle = ((ShoppingItemViewModel) model).getDate() + " " + ((ShoppingItemViewModel) model).getNote();

            ((LayoutStatisticsBinding) mBinder).lsBasketTitle.setText(getBasketTitle());
            if (mBasketAdapter != null) {
                ((BasketListAdapter) mBasketAdapter).refreshAll();
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.setAdapter(mShoppingListAdapter);

        DividerItemDecoration divider = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL);
        divider.setDrawable(mActivity.getDrawable(R.drawable.horizontal_line));
        recyclerView.addItemDecoration(divider);

        ItemTouchHelper.Callback swipeCallback = new SwipeItemTouchHelperCallback
                .Builder(0, ItemTouchHelper.LEFT)
                .bgColorSwipeLeft(bgColorSwipeLeft)
                .bgColorSwipeRight(bgColorSwipeRight)
                .drawableSwipeLeft(drawableSwipeLeft)
                .drawableSwipeRight(drawableSwipeRight)
                .setSwipeEnabled(swipeEnabled)
                .onItemSwipeLeftListener(position -> {
                    new AlertDialog.Builder(ApplicationLifecycleService.getCurrentActivity(), R.style.AlertDialogStyle)
                            .setTitle(R.string.dd_title)
                            .setMessage(R.string.dd_message).setPositiveButton(R.string.ok, (dialogInterface, i) -> {
                        dialogInterface.dismiss();
                        Database.list().removeShoppingList(mShoppingListItems.get(position).getId());
                        ((ShoppingListAdapter) mShoppingListAdapter).remove(position);
                    }).setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
                        dialogInterface.dismiss();
                        ((ShoppingListAdapter) mShoppingListAdapter).refresh(position);
                    }).setCancelable(false).show();
                })
                .build();

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(swipeCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    private void bindBasketList(RecyclerView recyclerView, boolean swipeEnabled, Drawable drawableSwipeLeft,
                                Drawable drawableSwipeRight, int bgColorSwipeLeft, int bgColorSwipeRight, Context context) {
        mBasketAdapter = new BasketListAdapter(listBasketItems());
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.setAdapter(mBasketAdapter);

        ItemTouchHelper.Callback swipeCallback = new SwipeItemTouchHelperCallback
                .Builder(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT)
                .bgColorSwipeLeft(bgColorSwipeLeft)
                .bgColorSwipeRight(bgColorSwipeRight)
                .drawableSwipeLeft(drawableSwipeLeft)
                .drawableSwipeRight(drawableSwipeRight)
                .setSwipeEnabled(swipeEnabled)

                .onItemSwipeLeftListener(position -> {
                    Database.list().removeItemFromList(mBasketItems.get(position).getId());
                    Database.list().updateBillInList(mBasketItems.get(position).getListId());
                    ((BasketListAdapter) mBasketAdapter).remove(position);


                })
                .onItemSwipeRightListener(position -> {
                    final EditShoppingItemDialog dialog = new EditShoppingItemDialog(
                            ApplicationLifecycleService.getCurrentActivity(),
                            mBasketItems.get(position).getId(),
                            mBasketItems.get(position).getName(),
                            Integer.valueOf(mBasketItems.get(position).getQuantity()),
                            Double.valueOf(mBasketItems.get(position).getPrice())
                    ).build();

                    dialog.show();
                    dialog.onCancel(() -> {
                        dialog.dismiss();
                        ((BasketListAdapter) mBasketAdapter).refresh(position);
                    });
                    dialog.onOk((id, title, quantity, price) -> {
                        dialog.dismiss();
                        mBasketItems.get(position).updateItem(id, title, price, quantity);
                        ((BasketListAdapter) mBasketAdapter).refresh(position);
                    });
                })
                .build();

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(swipeCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);

    }

    private void fillCartHeader(ShoppingItemViewModel model) {
        ((LayoutStatisticsBinding) mBinder).lsCartDate.setText(model.getDate());
        ((LayoutStatisticsBinding) mBinder).lsCartNote.setText(model.getNote());
        ((LayoutStatisticsBinding) mBinder).lsCartItems.setText(model.getItems());
        ((LayoutStatisticsBinding) mBinder).lsCartBudget.setText(model.getBudget());
        ((LayoutStatisticsBinding) mBinder).lsCartBill.setText(model.getBill());
        ((LayoutStatisticsBinding) mBinder).lsCartSum.setText(model.getSummary());
        ((LayoutStatisticsBinding) mBinder).lsCartItem.setBackgroundColor(
                ContextCompat.getColor(mActivity,
                        model.isOnBudget() ? R.color.colorInBudget : R.color.colorOutOfBudget));
        ((LayoutStatisticsBinding) mBinder).lsCartSum.setTextColor(
                ContextCompat.getColor(mActivity,
                        model.isOnBudget() ? R.color.colorGreen : R.color.colorPrimary)
        );

    }

    public void onCartHeaderClicked() {
        Animations.visibility(((LayoutStatisticsBinding) mBinder).lsMonthList, View.GONE);
        Animations.visibility(((LayoutStatisticsBinding) mBinder).lsCartItem, View.GONE);
        Animations.visibility(((LayoutStatisticsBinding) mBinder).lsBasketList, View.GONE);
        Animations.visibility(((LayoutStatisticsBinding) mBinder).lsShoppingList, View.VISIBLE);
        Animations.visibility(((LayoutStatisticsBinding) mBinder).lsMonthItem, View.VISIBLE);
        ((LayoutStatisticsBinding) mBinder).lsBasketTitle.setText("");
        mBasketItems.clear();
        mBasketTitle = null;
    }

    public void onMonthHeaderClicked() {
        mShoppingListItems.clear();
        Animations.visibility(((LayoutStatisticsBinding) mBinder).lsMonthList, View.VISIBLE);
        Animations.visibility(((LayoutStatisticsBinding) mBinder).lsBasketList, View.GONE);
        Animations.visibility(((LayoutStatisticsBinding) mBinder).lsCartItem, View.GONE);
        Animations.visibility(((LayoutStatisticsBinding) mBinder).lsShoppingList, View.GONE);
        Animations.visibility(((LayoutStatisticsBinding) mBinder).lsMonthItem, View.GONE);
        ((LayoutStatisticsBinding) mBinder).lsMonth.setText("");
        ((LayoutStatisticsBinding) mBinder).lsBasketTitle.setText("");
        mMonth = null;
        mBasketTitle = null;
    }

    private void fillMonthHeader(DefaultStatItemViewModel model) {
        ((LayoutStatisticsBinding) mBinder).lsMonthItemMonth.setText(model.getMonth());
        ((LayoutStatisticsBinding) mBinder).lsMonthItemNames.setText(model.getNames());
        ((LayoutStatisticsBinding) mBinder).lsMonthItemCount.setText(model.getCount());
        ((LayoutStatisticsBinding) mBinder).lsMonthItemBudget.setText(model.getBudget());
        ((LayoutStatisticsBinding) mBinder).lsMonthItemSpents.setText(model.getSpents());
        ((LayoutStatisticsBinding) mBinder).lsMonthItemSum.setText(model.getSummary());
        ((LayoutStatisticsBinding) mBinder).lsMonthItem.setBackgroundColor(
                ContextCompat.getColor(mActivity,
                        model.isOnBudget() ? R.color.colorInBudget : R.color.colorOutOfBudget));
        ((LayoutStatisticsBinding) mBinder).lsMonthItemSum.setTextColor(
                ContextCompat.getColor(mActivity,
                        model.isOnBudget() ? R.color.colorGreen : R.color.colorPrimary)
        );

    }

    @Override
    public ListBindingComponent getListBindingComponent() {
        return this;
    }

    @Override
    public SwipeBindingComponent getSwipeBindingComponent() {
        return this;
    }
}
