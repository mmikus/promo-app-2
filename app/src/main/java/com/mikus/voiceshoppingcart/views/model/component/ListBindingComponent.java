package com.mikus.voiceshoppingcart.views.model.component;

import android.content.Context;
import android.widget.ListView;

import androidx.databinding.BindingAdapter;

/**
 * Created by MMIIT on 2/25/2020.
 */
public interface ListBindingComponent {
    @BindingAdapter({"bind:items", "bind:context"})
    void bindList(ListView view, Object list, Context context);
}
