package com.mikus.voiceshoppingcart.views.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.mikus.voiceshoppingcart.BR;
import com.mikus.voiceshoppingcart.views.model.AbstractViewModel;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ViewDataBinding;

/**
 * Created by MMIIT on 2/26/2020.
 */
public abstract class AbstractAdapter<T extends AbstractViewModel> extends BaseAdapter {

    protected ObservableArrayList<T> mModel;
    protected Context mContext;

    public AbstractAdapter(ObservableArrayList<T> model, Context context) {
        this.mModel = model;
        this.mContext = context;
    }

    public AbstractAdapter(ObservableArrayList<T> model) {
        this.mModel = model;
    }

    @Override
    public int getCount() {
        return mModel.size();
    }

    @Override
    public Object getItem(int i) {
        return mModel.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) viewGroup.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewDataBinding binding = DataBindingUtil
                .inflate(inflater, mModel.get(i).getLayoutId(), viewGroup, false, mModel.get(i));
        binding.setVariable(BR.model, mModel.get(i));
        mModel.get(i).setBinder(binding);
        return binding.getRoot();
    }
}
