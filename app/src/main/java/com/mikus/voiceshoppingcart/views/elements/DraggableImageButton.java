package com.mikus.voiceshoppingcart.views.elements;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;

import com.mikus.voiceshoppingcart.utils.SpeechRecognizer;

/**
 * Created by MMIIT on 4/23/2020.
 */
public class DraggableImageButton extends ImageButton implements View.OnTouchListener {
    private static final int MAX_CLICK_DURATION = 250;
    private long pressStartTime;
    float dX;
    float dY;
    private IOnClickListener mClickListener;
    private IOnSavePositionListener mSavePositionListener;

    public DraggableImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DraggableImageButton setOnSavePositionListener(IOnSavePositionListener listener) {
        this.mSavePositionListener = listener;
        return this;
    }

    public DraggableImageButton setOnClickListener(IOnClickListener listener) {
        this.mClickListener = listener;
        this.setOnTouchListener(this);
        return this;
    }

    @Deprecated
    public DraggableImageButton initPosition(Float x, Float y) {
        return this;
    }

    public DraggableImageButton hasRecorder() {
        return this;
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                pressStartTime = System.currentTimeMillis();
                dX = view.getX() - event.getRawX();
                dY = view.getY() - event.getRawY();
                break;

            case MotionEvent.ACTION_MOVE:
                view.setY(event.getRawY() + dY);
                view.setX(event.getRawX() + dX);
                break;

            case MotionEvent.ACTION_UP:
                long pressDuration = System.currentTimeMillis() - pressStartTime;
                if (pressDuration < MAX_CLICK_DURATION) {
                    if (mClickListener != null) {
                        mClickListener.onClick();
                    }
                } else {
                    if (mSavePositionListener != null) {
                        mSavePositionListener.onSavePosition(view.getX(), view.getY());
                    }
                }
                break;

            default:
                return false;
        }
        return true;
    }

    private float distance(float x1, float y1, float x2, float y2) {
        float dx = x1 - x2;
        float dy = y1 - y2;
        float distanceInPx = (float) Math.sqrt(dx * dx + dy * dy);
        return pxToDp(distanceInPx);
    }

    private float pxToDp(float px) {
        return px / getResources().getDisplayMetrics().density;
    }

    public interface IOnClickListener {
        void onClick();
    }

    public interface IOnSavePositionListener {
        void onSavePosition(float x, float y);
    }
}
