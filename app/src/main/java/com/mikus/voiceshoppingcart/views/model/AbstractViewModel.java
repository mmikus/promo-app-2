package com.mikus.voiceshoppingcart.views.model;

import com.mikus.voiceshoppingcart.views.model.component.ListBindingComponent;
import com.mikus.voiceshoppingcart.views.model.component.SwipeBindingComponent;

import org.parceler.Transient;

import androidx.databinding.BaseObservable;
import androidx.databinding.ViewDataBinding;
import androidx.databinding.DataBindingComponent;

/**
 * Created by MMIIT on 2/23/2020.
 */
public abstract class AbstractViewModel extends BaseObservable implements DataBindingComponent {
    @Transient
    protected ViewDataBinding mBinder;

    public abstract String getModelId();

    public abstract int getLayoutId();

    public void setBinder(ViewDataBinding binder) {
        this.mBinder = binder;
    }

    @Override
    public ListBindingComponent getListBindingComponent() {
        return null;
    }

    @Override
    public SwipeBindingComponent getSwipeBindingComponent(){return null;}
}
