package com.mikus.voiceshoppingcart.views.model;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;

import com.mikus.voiceshoppingcart.R;
import com.mikus.voiceshoppingcart.activities.dashboard.DashboardActivity;
import com.mikus.voiceshoppingcart.activities.list.ShoppingListActivity;
import com.mikus.voiceshoppingcart.data.Database;
import com.mikus.voiceshoppingcart.dialogs.AddNewListDialog;
import com.mikus.voiceshoppingcart.dialogs.EditBudgetDialog;
import com.mikus.voiceshoppingcart.dialogs.EditNameDialog;
import com.mikus.voiceshoppingcart.services.ApplicationLifecycleService;
import com.mikus.voiceshoppingcart.utils.Alerts;
import com.mikus.voiceshoppingcart.utils.Animations;
import com.mikus.voiceshoppingcart.utils.Nulls;
import com.mikus.voiceshoppingcart.utils.PrettyDateUtil;
import com.mikus.voiceshoppingcart.utils.Wizard;
import com.mikus.voiceshoppingcart.views.adapter.ShoppingListAdapter;
import com.mikus.voiceshoppingcart.views.model.component.SwipeBindingComponent;
import com.mikus.voiceshoppingcart.views.elements.SwipeItemTouchHelperCallback;

import org.parceler.Transient;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Locale;

import androidx.databinding.Bindable;
import androidx.databinding.ObservableArrayList;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by MMIIT on 2/23/2020.
 */
public class DashboardViewModel extends AbstractViewModel implements SwipeBindingComponent {

    private int mLayoutId;
    private ObservableArrayList<ShoppingItemViewModel> mItems;
    private RecyclerView.Adapter mAdapter;
    @Transient
    private Activity mActivity;

    public DashboardViewModel(int layoutId, Activity activity) {
        this.mLayoutId = layoutId;
        this.mActivity = activity;
    }

    public DashboardViewModel(int mLayoutId) {
        this.mLayoutId = mLayoutId;
    }

    private String mName;
    private Double mBudget;
    private Double mSpents;

    @Bindable
    public String getName() {
        if (mActivity != null) {
            mName = Database.account().readName();
            String pattern = mActivity.getString(R.string.dl_stat_text);
            try {
                return pattern.replace("%name%", mName).replace("%month%", PrettyDateUtil.getActualMonthLocalized());
            } catch (ParseException e) {
                return "";
            }
        }
        return "";
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    @Bindable
    public String getMonth() {
        String pattern = mActivity.getString(R.string.dl_stat_month);
        try {
            return pattern.replace("%month%", PrettyDateUtil.getActualMonthLocalized());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Bindable
    public String getBudget() {
        if (mBudget == null) {
            mBudget = Database.account().readBudget();
        }
        return mBudget == 0f ? "0.0" : mBudget.toString();
    }

    @Bindable
    public String getSpents() {
        if (mSpents == null) {
            mSpents = Database.stat().getSpentsByCurrentMonth();
        }
        return String.format(Locale.US, "-%.2f", mSpents);
    }

    @Bindable
    public String getSumarization() {
        return String.format(Locale.US, "%.2f", Double.valueOf(getBudget()) + Double.valueOf(getSpents()));
    }

    @Bindable
    public boolean isOnBudget() {
        return Double.valueOf(getSumarization()) >= 0;
    }

    public void setBudget(String mBudget) {
        this.mBudget = Nulls.isNull(mBudget) ? 0d : Double.valueOf(mBudget);

    }

    @Override
    public String getModelId() {
        return DashboardViewModel.class.getName();
    }

    @Override
    public int getLayoutId() {
        return mLayoutId;
    }


    public ObservableArrayList<ShoppingItemViewModel> listShoppingCarts() {
        if (mItems == null) {
            mItems = Database.list().listShoppingCarts();
        }
        return mItems;
    }

    public void onSaveAccount(Context context) {
        if (!Nulls.isNull(mName) && !Nulls.isNull(mBudget)) {
            Database.account().createAccount(mName, Double.valueOf(mBudget));
            Intent intent = new Intent(context, DashboardActivity.class);
            context.startActivity(intent);

        } else {
            Alerts.error(R.string.error_welcome_page_inputs_not_filled);
        }
    }


    public void onAddNewList(Context context) {
        Wizard.hide(Wizard.DASHBOARD_STEP);
        final AddNewListDialog dialog = new AddNewListDialog(context).build();
        dialog.show();

        dialog.onCancel(() -> {
            dialog.dismiss();
        });
        dialog.onOk((note, budget) -> {
            dialog.dismiss();
            ShoppingItemViewModel model = Database.list().addNewList(note, budget);
            ((ShoppingListAdapter) mAdapter).add(model);
            Animations.showSubActivity(ShoppingListActivity.class, DashboardActivity.class, new HashMap<String, Object>() {{
                put("list_id", model.getBasketId());
            }});
        });


    }

    @Override
    public void setItemSwipeToRecyclerView(RecyclerView recyclerView, boolean swipeEnabled, Drawable drawableSwipeLeft,
                                           Drawable drawableSwipeRight, int bgColorSwipeLeft, int bgColorSwipeRight, Context context) {

        mAdapter = new ShoppingListAdapter(listShoppingCarts(), model -> {
            int id = ((ShoppingItemViewModel) model).getBasketId();
            Animations.showSubActivity(ShoppingListActivity.class, DashboardActivity.class, new HashMap<String, Object>() {{
                put("list_id", id);
            }});

        });
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.setAdapter(mAdapter);
        DividerItemDecoration divider = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL);
        divider.setDrawable(mActivity.getDrawable(R.drawable.horizontal_line));
        recyclerView.addItemDecoration(divider);

        ItemTouchHelper.Callback swipeCallback = new SwipeItemTouchHelperCallback
                .Builder(0, ItemTouchHelper.LEFT)
                .bgColorSwipeLeft(bgColorSwipeLeft)
                .bgColorSwipeRight(bgColorSwipeRight)
                .drawableSwipeLeft(drawableSwipeLeft)
                .drawableSwipeRight(drawableSwipeRight)
                .setSwipeEnabled(swipeEnabled)
                .onItemSwipeLeftListener(position -> {
                    new AlertDialog.Builder(ApplicationLifecycleService.getCurrentActivity(), R.style.AlertDialogStyle)
                            .setTitle(R.string.dd_title)
                            .setMessage(R.string.dd_message).setPositiveButton(R.string.ok, (dialogInterface, i) -> {
                        dialogInterface.dismiss();
                        Database.list().removeShoppingList(mItems.get(position).getId());
                        ((ShoppingListAdapter) mAdapter).remove(position);
                    }).setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
                        dialogInterface.dismiss();
                        ((ShoppingListAdapter) mAdapter).refresh(position);
                    }).setCancelable(false).show();

                })
                .build();

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(swipeCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);

    }

    public void onBudgetEditClicked() {
        final EditBudgetDialog dialog = new EditBudgetDialog(ApplicationLifecycleService.getCurrentActivity(), mBudget).build();
        dialog.show();
        dialog.onCancel(() -> dialog.dismiss());
        dialog.onOk((id, budget) -> {
            dialog.dismiss();
            Database.account().updateBudget(budget);
            mBudget = budget;
            notifyChange();
        });

    }

    public void onNameEditClicked() {
        final EditNameDialog dialog = new EditNameDialog(ApplicationLifecycleService.getCurrentActivity(), mName).build();
        dialog.show();
        dialog.onCancel(() -> dialog.dismiss());
        dialog.onOk((name) -> {
            dialog.dismiss();
            Database.account().updateName(name);
            mName = name;
            notifyChange();
        });

    }

    @Override
    public SwipeBindingComponent getSwipeBindingComponent() {
        return this;
    }
}
