package com.mikus.voiceshoppingcart.views.model.component;

import android.content.Context;
import android.graphics.drawable.Drawable;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by MMIIT on 3/29/2020.
 */
public interface SwipeBindingComponent {
    @BindingAdapter(value = {"swipeEnabled", "drawableSwipeLeft", "drawableSwipeRight", "bgColorSwipeLeft", "bgColorSwipeRight", "context"}, requireAll = false)
    void setItemSwipeToRecyclerView(RecyclerView recyclerView, boolean swipeEnabled,
                                    Drawable drawableSwipeLeft, Drawable drawableSwipeRight, int bgColorSwipeLeft, int bgColorSwipeRight, Context context);
}
