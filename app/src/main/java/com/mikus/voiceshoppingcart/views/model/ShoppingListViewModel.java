package com.mikus.voiceshoppingcart.views.model;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;

import android.graphics.drawable.Drawable;

import com.mikus.voiceshoppingcart.R;
import com.mikus.voiceshoppingcart.data.Database;
import com.mikus.voiceshoppingcart.databinding.LayoutShoppingListBinding;
import com.mikus.voiceshoppingcart.dialogs.EditBudgetDialog;
import com.mikus.voiceshoppingcart.dialogs.EditShoppingItemDialog;
import com.mikus.voiceshoppingcart.dialogs.ListNoteDialog;
import com.mikus.voiceshoppingcart.services.ApplicationLifecycleService;
import com.mikus.voiceshoppingcart.utils.Alerts;
import com.mikus.voiceshoppingcart.utils.ConnectionUtil;
import com.mikus.voiceshoppingcart.utils.Nulls;
import com.mikus.voiceshoppingcart.utils.SpeechRecognizer;
import com.mikus.voiceshoppingcart.utils.Wizard;
import com.mikus.voiceshoppingcart.views.adapter.BasketListAdapter;
import com.mikus.voiceshoppingcart.views.model.component.SwipeBindingComponent;
import com.mikus.voiceshoppingcart.views.elements.SwipeItemTouchHelperCallback;

import org.parceler.Transient;

import java.util.Locale;

import androidx.core.view.GravityCompat;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableArrayList;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


/**
 * Created by MMIIT on 2/25/2020.
 */
public class ShoppingListViewModel extends AbstractViewModel implements SwipeBindingComponent, BasketItemViewModel.IBasketItemChangedListener {

    private final static int DEFAULT_TRY_COUNTER = 4;
    private int mLayoutId;
    private int mListId;
    private ObservableArrayList<BasketItemViewModel> mItems;
    private RecyclerView.Adapter mAdapter;
    private SpeechRecognizer mRecognizer;
    private int mTryCounter = DEFAULT_TRY_COUNTER;
    private String mDate;
    private String mName;
    private Double mBudget;
    private Double mSpents;

    @Transient
    private Activity mActivity;

    public ShoppingListViewModel(int mLayoutId, int listId, Activity activity) {
        this.mLayoutId = mLayoutId;
        this.mListId = listId;
        this.mActivity = activity;
    }


    @Bindable
    public String getName() {
        if (mName == null) {
            mName = Database.list().getShoppingListNote(mListId);
        }
        if (Nulls.isNull(mName)) {
            mName = Database.stat().getBasketDate(mListId);
        }
        return mName;

    }

    public void setName(String mName) {
        this.mName = mName;
    }

    @Bindable
    public String getDate() {
        if (mDate == null) {
            mDate = Database.stat().getBasketDate(mListId);
        }
        if (getName().equals(mDate)) {
            return "";
        }
        return mDate;
    }

    @Bindable
    public String getBudget() {
        if (mBudget == null) {
            mBudget = Database.stat().getBudgetByBasketId(mListId);
        }
        return mBudget.toString();
    }

    @Bindable
    public String getSpents() {
        if (mSpents == null) {
            mSpents = Database.stat().getSpentsByBasketId(mListId);
        }
        return String.format(Locale.US, "-%.2f", mSpents);
    }

    @Bindable
    public String getSumarization() {
        return String.format(Locale.US, "%.2f", Double.valueOf(getBudget()) + Double.valueOf(getSpents()));
    }

    @Bindable
    public boolean isOnBudget() {
        return Double.valueOf(getSumarization()) >= 0;
    }

    @Bindable
    public String getListName() {
        mItems = listItems();

        //long inBasketCount = CollectionUtils.countMatches(mItems, object -> object.isInBasket());
        //long inBasketCount = CollectionsUtils.count(mItems, i -> isOnBudget());
        long inBasketCount = mItems.stream().filter(i -> i.isInBasket()).count();
        return String.format("%s (%d/%d)", mActivity.getString(R.string.sl_header_name),
                inBasketCount, mItems.size());
    }

    @Override
    public String getModelId() {
        return ShoppingListViewModel.class.getName();
    }

    @Override
    public int getLayoutId() {
        return mLayoutId;
    }

    public ObservableArrayList<BasketItemViewModel> listItems() {
        if (mItems == null) {
            mItems = Database.list().listItems(mListId, this);
        }
        return mItems;
    }

    @Override
    public void setItemSwipeToRecyclerView(RecyclerView recyclerView, boolean swipeEnabled, Drawable drawableSwipeLeft,
                                           Drawable drawableSwipeRight, int bgColorSwipeLeft, int bgColorSwipeRight, Context context) {
        mAdapter = new BasketListAdapter(listItems());
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.setAdapter(mAdapter);
        ItemTouchHelper.Callback swipeCallback = new SwipeItemTouchHelperCallback
                .Builder(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT)
                .bgColorSwipeLeft(bgColorSwipeLeft)
                .bgColorSwipeRight(bgColorSwipeRight)
                .drawableSwipeLeft(drawableSwipeLeft)
                .drawableSwipeRight(drawableSwipeRight)
                .setSwipeEnabled(swipeEnabled)
                .onItemSwipeLeftListener(position -> {
                    Database.list().removeItemFromList(mItems.get(position).getId());
                    ((BasketListAdapter) mAdapter).remove(position);
                    notifyChanged();
                    Database.list().updateBillInList(mListId);
                })
                .onItemSwipeRightListener(position -> {
                    final EditShoppingItemDialog dialog = new EditShoppingItemDialog(
                            ApplicationLifecycleService.getCurrentActivity(),
                            mItems.get(position).getId(),
                            mItems.get(position).getName(),
                            Integer.valueOf(mItems.get(position).getQuantity()),
                            Double.valueOf(mItems.get(position).getPrice())
                    ).build();

                    dialog.show();
                    dialog.onCancel(() -> {
                        dialog.dismiss();
                        ((BasketListAdapter) mAdapter).refresh(position);
                    });
                    dialog.onOk((id, title, quantity, price) -> {
                        dialog.dismiss();
                        mItems.get(position).updateItem(id, title, price, quantity);
                        ((BasketListAdapter) mAdapter).refresh(position);
                        notifyChanged();
                    });
                })
                .build();


        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(swipeCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    @Override
    public SwipeBindingComponent getSwipeBindingComponent() {
        return this;
    }

    public void onAddItem() {
        new ConnectionUtil().setConnectionCheckedListener(new ConnectionUtil.IConnectionCheckedListener() {
            @Override
            public void onConnected() {
                mRecognizer = new SpeechRecognizer();
                mRecognizer.setOnWordRecognized(text -> {
                    BasketItemViewModel item = Database.list().addItemToList(mListId, text, () -> notifyChanged());
                    if (item != null) {
                        Wizard.hide(Wizard.ADD_ITEM_TO_LIST_STEP);
                        ((BasketListAdapter) mAdapter).add(item);
                        notifyChange();
                        Wizard.run(Wizard.SWIPE_BASKET_ITEM_STEP, ((LayoutShoppingListBinding) mBinder).wizardLayoutListSteps,
                                ((LayoutShoppingListBinding) mBinder).wizardItemListLayout, 500);

                    }
                });
                mRecognizer.setOnErrorListener(() -> tryCounter(--mTryCounter));
                mRecognizer.start();
            }

            @Override
            public void onDisconnected() {
                Alerts.error(R.string.error_no_internet_connection);
                showEditDialog();
            }
        }).check();
    /*    if (Permissions.hasInternetConnection()) {
            mRecognizer = new SpeechRecognizer();
            mRecognizer.setOnWordRecognized(text -> {
                BasketItemViewModel item = Database.list().addItemToList(mListId, text, () -> notifyChanged());
                if (item != null) {
                    Wizard.hide(Wizard.ADD_ITEM_TO_LIST_STEP);
                    ((BasketListAdapter) mAdapter).add(item);
                    notifyChange();
                    Wizard.run(Wizard.SWIPE_BASKET_ITEM_STEP, ((LayoutShoppingListBinding) mBinder).wizardLayoutListSteps,
                            ((LayoutShoppingListBinding) mBinder).wizardItemListLayout, 500);

                }
            });
            mRecognizer.setOnErrorListener(() -> tryCounter(--mTryCounter));
            mRecognizer.start();
        } else {
            Alerts.error(R.string.error_no_internet_connection);
            showEditDialog();
        }*/
    }

    public void onNoteEditClicked() {
        final ListNoteDialog dialog = new ListNoteDialog(ApplicationLifecycleService.getCurrentActivity(), mListId, mName).build();
        dialog.show();
        dialog.onCancel(() -> dialog.dismiss());
        dialog.onOk((id, note) -> {
            dialog.dismiss();
            Database.list().updateNoteInList(id, note);
            mName = note;
            notifyChange();
        });

    }

    public void onBudgetEditClicked() {
        final EditBudgetDialog dialog = new EditBudgetDialog(ApplicationLifecycleService.getCurrentActivity(), mListId, mBudget).build();
        dialog.show();
        dialog.onCancel(() -> dialog.dismiss());
        dialog.onOk((id, budget) -> {
            dialog.dismiss();
            Database.list().updateBudgetInList(id, budget);
            mBudget = budget;
            notifyChange();
        });

    }

    @Override
    public void onItemChanged() {
        notifyChanged();
    }

    private void notifyChanged() {
        mBudget = Database.stat().getBudgetByBasketId(mListId);
        mSpents = Database.stat().getSpentsByBasketId(mListId);

        mItems.sort((t, t1) -> Boolean.compare(!t.isInBasket(), !t1.isInBasket()));
        ((BasketListAdapter) mAdapter).refreshAll();
        notifyChange();

    }

    private void tryCounter(int count) {
        if (count < 0) {
            new AlertDialog.Builder(ApplicationLifecycleService.getCurrentActivity(), R.style.AlertDialogStyle)
                    .setTitle(R.string.oqd_title)
                    .setMessage(R.string.oqd_message).setPositiveButton(R.string.ok, (dialogInterface, i) -> {
                dialogInterface.dismiss();
                showEditDialog();
            }).setNegativeButton(R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss())
                    .show();
            mTryCounter = DEFAULT_TRY_COUNTER;
        }
    }

    private void showEditDialog() {
        if (mRecognizer != null) {
            mRecognizer.stop();
        }
        final EditShoppingItemDialog dialog = new EditShoppingItemDialog(
                ApplicationLifecycleService.getCurrentActivity(),
                mListId,
                "",
                0,
                0
        ).build();
        dialog.show();
        dialog.onCancel(() -> {
            dialog.dismiss();
        });
        dialog.onOk((id, title, quantity, price) -> {
            dialog.dismiss();
            BasketItemViewModel item = Database.list().addItemToList(mListId, title, price, quantity, () -> notifyChanged());
            if (item != null) {
                ((BasketListAdapter) mAdapter).add(item);
            }
        });
    }

    public void showBasketItemHelp() {

        if (Database.account().getWizartStep() == Wizard.DRAWER_MENU_STEP) {
            Wizard.hide(Wizard.DONE);
        } else if (Database.account().getWizartStep() == Wizard.SUMMARIZATION_STEP) {
            Wizard.hide(Wizard.DRAWER_MENU_STEP);
            ((DrawerLayout) ApplicationLifecycleService.getCurrentActivity().findViewById(R.id.drawer_layout)).closeDrawer(GravityCompat.START);
            Wizard.run(Wizard.DONE, ((LayoutShoppingListBinding) mBinder).wizardDoneLayout);
        } else if (Database.account().getWizartStep() == Wizard.BASKET_ITEM_HELP_STEP) {
            Wizard.hide(Wizard.SUMMARIZATION_STEP);
            Wizard.run(Wizard.DRAWER_MENU_STEP, ((LayoutShoppingListBinding) mBinder).wizardDrawerLayout);
            ((DrawerLayout) ApplicationLifecycleService.getCurrentActivity().findViewById(R.id.drawer_layout)).openDrawer(GravityCompat.START);
        } else if (Database.account().getWizartStep() == Wizard.SWIPE_BASKET_ITEM_STEP) {
            Wizard.hide(Wizard.BASKET_ITEM_HELP_STEP);
            Wizard.run(Wizard.SUMMARIZATION_STEP, ((LayoutShoppingListBinding) mBinder).wizardSumarizationLayout);
        } else {
            Wizard.hide(Wizard.SWIPE_BASKET_ITEM_STEP);
            Wizard.run(Wizard.BASKET_ITEM_HELP_STEP,
                    ((LayoutShoppingListBinding) mBinder).wizardLayoutListSteps,
                    ((LayoutShoppingListBinding) mBinder).wizardStepTitle,
                    ((LayoutShoppingListBinding) mBinder).wizardAboveItemHelp,
                    ((LayoutShoppingListBinding) mBinder).wizardBelowItemHelp,
                    ((LayoutShoppingListBinding) mBinder).wizardItemListLayout
            );
        }
    }
}


