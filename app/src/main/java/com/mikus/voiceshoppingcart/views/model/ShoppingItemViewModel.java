package com.mikus.voiceshoppingcart.views.model;

import com.mikus.voiceshoppingcart.R;
import com.mikus.voiceshoppingcart.utils.Nulls;

import java.util.Locale;

import androidx.databinding.Bindable;

/**
 * Created by MMIIT on 2/25/2020.
 */
public class ShoppingItemViewModel extends AbstractViewModel {

    private int mBasketId;
    private String mDate;
    private String mItems;
    private Double mBudget;
    private Double mBill;
    private String mNote;

    public ShoppingItemViewModel() {
    }

    public ShoppingItemViewModel(int basketId, String mDate, String mItems, Double mBudget, Double mBill, String note) {
        this.mDate = mDate;
        this.mItems = mItems;
        this.mBudget = mBudget;
        this.mBill = mBill;
        this.mBasketId = basketId;
        this.mNote = note;
    }

    public int getBasketId() {
        return mBasketId;
    }

    @Bindable
    public String getDate() {
        return mDate;
    }

    @Bindable
    public String getItems() {
        return mItems;
    }

    @Bindable
    public String getBudget() {
        return mBudget.toString();
    }

    @Bindable
    public String getBill() {
        return String.format(Locale.US, "-%.2f",mBill);
    }

    @Bindable
    public String getSummary() {
        return String.format(Locale.US,"%.2f", mBudget - mBill);
    }

    @Bindable
    public String getNote() {
        return Nulls.isNull(mNote) ? "" : "(" + mNote + ")";
    }

    public int getId() {
        return mBasketId;
    }

    @Bindable
    public boolean isOnBudget() {
        return (mBudget - mBill) >= 0;
    }

    @Override
    public String getModelId() {
        return ShoppingItemViewModel.class.getName();
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_layout_shopping_list;
    }
}
