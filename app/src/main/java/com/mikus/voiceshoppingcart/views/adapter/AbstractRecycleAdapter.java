package com.mikus.voiceshoppingcart.views.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mikus.voiceshoppingcart.views.model.AbstractViewModel;
import com.mikus.voiceshoppingcart.views.listeners.OnRecycleItemClickListener;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by MMIIT on 3/29/2020.
 */
public abstract class AbstractRecycleAdapter<T extends AbstractViewModel> extends RecyclerView.Adapter<AbstractRecycleAdapter.ViewHolder> {

    protected ObservableArrayList<T> mModel;
    protected Context mContext;
    private OnRecycleItemClickListener mClickListener;

    public AbstractRecycleAdapter(ObservableArrayList<T> model) {
        this.mModel = model;
    }

    public AbstractRecycleAdapter(ObservableArrayList<T> model, OnRecycleItemClickListener clickListener) {
        this.mModel = model;
        this.mClickListener = clickListener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        return new ViewHolder(DataBindingUtil
                .inflate(inflater, mModel.get(viewType).getLayoutId(), parent, false, mModel.get(viewType)));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.mBinder.setVariable(com.mikus.voiceshoppingcart.BR.model, mModel.get(position));
        mModel.get(position).setBinder(holder.mBinder);

        if (mClickListener != null) {
            holder.mBinder.getRoot().setOnClickListener(view -> {
                mClickListener.onItemClick(mModel.get(position));
            });
        }
    }

    public void remove(int position) {
        mModel.remove(position);
        notifyItemRemoved(position);
    }

    public void add(T model) {
        mModel.add(model);
        notifyItemInserted(mModel.size());
    }

    public void refresh(int position) {
        notifyItemChanged(position);
    }

    public void refreshAll() {
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mModel.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ViewDataBinding mBinder;

        public ViewHolder(ViewDataBinding binder) {
            super(binder.getRoot());
            this.mBinder = binder;
        }

        public ViewDataBinding getBinder() {
            return mBinder;
        }
    }
}
