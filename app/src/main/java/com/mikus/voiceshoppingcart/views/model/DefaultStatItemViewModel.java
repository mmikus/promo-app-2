package com.mikus.voiceshoppingcart.views.model;

import com.mikus.voiceshoppingcart.R;
import com.mikus.voiceshoppingcart.utils.PrettyDateUtil;

import java.util.Date;
import java.text.ParseException;
import java.util.Locale;

import androidx.databinding.Bindable;

/**
 * Created by MMIIT on 4/15/2020.
 */
public class DefaultStatItemViewModel extends AbstractViewModel {

    private Integer mMonth;
    private Integer mCount;
    private Double mBudget;
    private Double mSpents;
    private String mNames;
    private Date mFrom;
    private Date mTo;

    public DefaultStatItemViewModel(Integer month, Integer count, Double budget, Double spents, String names, Date from, Date to) {
        this.mMonth = month;
        this.mCount = count;
        this.mBudget = budget;
        this.mSpents = spents;
        this.mNames = names;
        this.mFrom = from;
        this.mTo = to;
    }

    public Date getFrom() {
        return mFrom;
    }

    public Date getTo() {
        return mTo;
    }

    @Bindable
    public String getMonth() {
        try {
            return PrettyDateUtil.formatMonth(mMonth);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Bindable
    public String getCount() {
        return mCount.toString();
    }

    @Bindable
    public String getBudget() {
        return mBudget.toString();
    }

    @Bindable
    public String getSpents() {
        return String.format(Locale.US, "-%.2f", mSpents);
    }

    @Bindable
    public String getSummary() {
        return String.format(Locale.US, "%.2f", mBudget - mSpents);
    }

    @Bindable
    public String getNames() {
        return mNames;
    }

    @Bindable
    public boolean isOnBudget() {
        return (mBudget - mSpents) > 0;
    }

    @Override
    public String getModelId() {
        return DefaultStatItemViewModel.class.getName();
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_layout_default_stat;
    }
}
