package com.mikus.voiceshoppingcart.views.adapter;

import com.mikus.voiceshoppingcart.views.model.ShoppingItemViewModel;
import com.mikus.voiceshoppingcart.views.listeners.OnRecycleItemClickListener;

import androidx.databinding.ObservableArrayList;

/**
 * Created by MMIIT on 2/26/2020.
 */
public class ShoppingListAdapter extends AbstractRecycleAdapter<ShoppingItemViewModel> {

    public ShoppingListAdapter(ObservableArrayList<ShoppingItemViewModel> model, OnRecycleItemClickListener listener) {
        super(model, listener);
    }

}
