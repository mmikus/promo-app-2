package com.mikus.voiceshoppingcart.views.listeners;

import com.mikus.voiceshoppingcart.views.model.AbstractViewModel;

/**
 * Created by MMIIT on 3/29/2020.
 */
public interface OnRecycleItemClickListener {
     void onItemClick(AbstractViewModel model);
}
